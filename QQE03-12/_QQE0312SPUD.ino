/*
QQE03-12SPUD
 */

// Pin Config
#define ledPin			13 
#define relayPin		9 
#define leftCurrentPin		A0     
#define rightCurrentPin		A1     

// Constants
#define startCurrent		30 
#define minCurrent		400	// 0.0049V per Units
#define maxCurrent		600	// 0.0049V per Units
#define heatTime		4000	// ~500/seconds !!!Attention the time must be enough to let the regulator going to the minimum voltage during the heat time!!!
#define highVoltageTime		1000	// ~500/seconds. Time starting the high voltage before the regulator begin to stabilize
#define stabilizationTime       7500	// ~500/seconds. Time required to the regulator to stabilize the current
#define switchOffset		122 	// 0.0049V per Units (0.6V)
#define regAverageCount		10	// Number of loop to average the current measures

// Internal use
long reg1CurrentSum = 0;		// Left channel regulator
long reg2CurrentSum = 0;		// Right channel regulator
int regLoopCount = 0;
float reg1CurrentAverage;
float reg2CurrentAverage;
int switchValue = 0;
int heatCount = 0;
int highVoltageCount = 0;
int stabilizationCount = 0;
int ledBlinkCount = 0;
int ledBlinkMax = 0;
int ledBlinkOn = 0;

// Errors
#define ERR_3  3      // 3: Current during heat time
#define ERR_4  4      // 4: Stabilization too long
#define ERR_5  5      // 6: Out of range during normal function
#define ERR_UNK  100  // Unknown error
int errorNumber = ERR_UNK;
int blinkErrorCount = 0;

// Sequence:
#define SEQ_DISCHARGE    0  // 0: Discharge
#define SEQ_HEAT         1  // 1: Heat tempo 
#define SEQ_STARTING     2  // 2: Starting High Voltage
#define SEQ_REGULATING   3  // 3: Waiting for reg
#define SEQ_FUNCTION     4  // 4: Normal Fonction
#define SEQ_FAIL         5  // 5: Fail
int sequence = SEQ_DISCHARGE;

void RelayOn(void) 
{
  digitalWrite(relayPin, HIGH);
}

void RelayOff(void) 
{
  digitalWrite(relayPin, LOW);
}

void LedOn(void) 
{
  digitalWrite(ledPin, HIGH); 
}

void LedOff(void) 
{
  digitalWrite(ledPin, LOW);    
}

void LedBlinking(void)
{
  ledBlinkCount++;
  if (ledBlinkMax == 0 || ledBlinkCount > ledBlinkMax)
  {
    LedOff();    
    ledBlinkCount = 0;
  }    
  else if (ledBlinkOn == ledBlinkMax)
  {
    LedOn();
    ledBlinkCount = 0;
  }
  else if (ledBlinkCount < ledBlinkOn)
  {
    LedOn();
  }
  else
  {
    LedOff();
  }
}

void BlinkLed(int enabledCount,int totalCount)
{
  ledBlinkMax = totalCount;
  ledBlinkOn = enabledCount;
}

boolean CheckInRange(int minValue, int maxValue)
{
  return reg1CurrentAverage > minValue && reg1CurrentAverage < maxValue && reg2CurrentAverage > minValue && reg2CurrentAverage < maxValue;
}

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);     
  pinMode(relayPin, OUTPUT);     
}

// the loop routine runs over and over again forever:
void loop() 
{ 
  reg1CurrentSum += analogRead(leftCurrentPin);
  reg2CurrentSum += analogRead(rightCurrentPin);
  regLoopCount++;

  if (regLoopCount > regAverageCount)
  {
    reg1CurrentAverage = reg1CurrentSum / regLoopCount;
    reg2CurrentAverage = reg2CurrentSum / regLoopCount;
    regLoopCount = 0;
    reg1CurrentSum = 0;
    reg2CurrentSum = 0;
  }

  LedBlinking();

  switch (sequence)
  {  
  case SEQ_DISCHARGE:	
    RelayOff();
    BlinkLed(800, 1000);

    if(regLoopCount != 0 || !CheckInRange(-100, startCurrent))
    {
      break;
    }

    sequence++;

  case SEQ_HEAT: 
    if(!CheckInRange(-100, startCurrent + 10))
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_3;
      break;
    }
    RelayOff();
    BlinkLed(400, 800);

    if(heatCount++ < heatTime)
    {
      break;
    }

    sequence++;
    LedOn();
    delay(2500);  

  case SEQ_STARTING:
    // Starting High Voltage
    BlinkLed(100, 100);
    RelayOn();

    if(highVoltageCount++ < highVoltageTime)
    {
      break;  
    }  

    sequence++;

  case SEQ_REGULATING: 
    // Waiting for regulator
    if(stabilizationCount++ > stabilizationTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_4;
      break;
    }

    BlinkLed(20, 500);

    if(!CheckInRange(minCurrent + 10, maxCurrent))
    {
      break;
    }

    sequence++;

  case SEQ_FUNCTION:
    // Normal Fonction, wait and see        
    if(!CheckInRange(minCurrent, maxCurrent))
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_5;
      break;
    }

    BlinkLed(0, 0);
    break;

  default: 
    // Fail, protect mode
    RelayOff();
    BlinkLed(50, 100);

    if (ledBlinkCount == 0)
    {
      blinkErrorCount++;
    }

    if (blinkErrorCount > errorNumber)
    {
      blinkErrorCount = 0;
      delay(500);
    }
  }  

  delay(1);  
}










