PLOT REPORT
-----------

Report File:        Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy (PCB - PLOT REPORT).txt
Report Written:     Sunday, January 29, 2017
Design Path:        Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy.pcb
Design Title:       
Created:            21.04.2016 20:23:10
Last Saved:         29.01.2017 13:00:01
Editing Time:       1772 min
Units:              mm (precision 1)


Device Settings
===============


Gerber Settings
===============

    Leading zero suppression.
    G01 assumed throughout.
    Line termination <*> <CR> <LF>.
    3.5 format absolute inches.
    Format commands defined in Gerber file.
    Aperture table defined in Gerber file.
    Hardware arcs allowed.


Drill Settings
==============

    Excellon Format 1    Format 3.5 absolute in inches.
    No zero suppression.


Plots Output
============

    Plot Job: "Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy.mop"


Gerber Output
=============

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Top Silkscreen.gbr

    Layers:
    ------------------
    Top Silkscreen

    D-Code Shape  (thou)
    -----------------------
    D10    Round 5.00
    D12    Round 10.00
    D87    Round 8.00

Gerber Output
=============

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Top Copper.gbr

    Layers:
    --------------
    Top Copper

    D-Code Shape  (thou)
    -----------------------------
    D10    Round 5.00
    D16    Square 100.00
    D17    Square 100.00
    D18    Round 60.00
    D19    Oval 60.00 X 110.00
    D71    Round 48.00
    D21    Round 31.50
    D75    Round 23.62
    D88    Round 118.11
    D89    Round 62.99

Gerber Output
=============

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Top Copper (Paste).gbr

    Layers:
    --------------
    Top Copper

    D-Code Shape  (thou)
    -----------------------

Gerber Output
=============

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Top Solder Mask.gbr

    Layers:
    -------------------
    Top Solder Mask

    D-Code Shape  (thou)
    -----------------------------
    D22    Round 131.98
    D25    Square 106.00
    D26    Square 106.00
    D27    Round 66.00
    D28    Oval 66.00 X 116.00
    D72    Round 54.00
    D90    Round 124.11
    D91    Round 68.99

Gerber Output
=============

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Bottom Copper.gbr

    Layers:
    -----------------
    Bottom Copper

    D-Code Shape  (thou)
    -----------------------------
    D10    Round 5.00
    D16    Square 100.00
    D17    Square 100.00
    D18    Round 60.00
    D19    Oval 60.00 X 110.00
    D71    Round 48.00
    D20    Round 50.00
    D21    Round 31.50
    D75    Round 23.62
    D88    Round 118.11
    D89    Round 62.99

Gerber Output
=============

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Bottom Copper (Paste).gbr

    Layers:
    -----------------
    Bottom Copper

    D-Code Shape  (thou)
    -----------------------

Gerber Output
=============

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Bottom Solder Mask.gbr

    Layers:
    ----------------------
    Bottom Solder Mask

    D-Code Shape  (thou)
    -----------------------------
    D22    Round 131.98
    D25    Square 106.00
    D26    Square 106.00
    D27    Round 66.00
    D28    Oval 66.00 X 116.00
    D72    Round 54.00
    D90    Round 124.11
    D91    Round 68.99

NC Drill Output
===============

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Drill Data - [Through Hole].drl

    Output:
    ---------------------------
    Plated Round Drill Holes
    Plated Slots
    Plated Board Outlines

    Tool  Size         Count   ID
    ------------------------------------
    T001  000.02400 in 2     M
    T002  000.03000 in 4     L
    T003  000.03200 in 72    F
    T004  000.04000 in 16    H
    T005  000.04331 in 5     S
    T006  000.04724 in 2     Z
    T007  000.12598 in 3     J
    ------------------------------------
    Total              104
    ------------------------------------

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Drill Data - [Through Hole] (Unplated).drl

    Output:
    -----------------------------
    Unplated Round Drill Holes
    Unplated Slots
    Unplated Board Outlines

    Tool  Size         Count   ID
    ------------------------------------
    T001  000.02500 in 1
    ------------------------------------
    Total              1
    ------------------------------------


Gerber Output
=============

Z:\Serge\Desktop\Khuntao\CDiff Guy\CDiff Guy - Drill Ident Drawing - [Through Hole].gbr

    Layers:
    ------------------
    [Through Hole]


Drill Ident Drawing
===================

    Drill Plated Size Shape ID
    -----------------------------------
    32    Y      100  Round F
    40    Y      100  Round H
    126   Y      100  Round J
    30    Y      100  Round L
    24    Y      100  Round M
    43    Y      100  Round S
    47    Y      100  Round Z

    D-Code Shape  (thou)
    -----------------------
    D10    Round 5.00
    D14    Round 100.00

End Of Report.
