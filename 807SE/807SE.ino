/*
807SE
 */

// Pin Config
int ledPin = 2;
int ledOnBoard = 13;
int relayPin = 10;
int regulatorRefPin = 3;
int indicatorPin = 5;
int aopFlag1 = 6; // Future usage
int aopFlag2 = 7; // Future usage
int leftCurrentPin = A1;    
int rightCurrentPin = A2;    
int switchPin = A3;    

// Constants
int biasRef = 160;               // 0(0V) to 255(5V)
int startCurrent = 30;           // 0.0049V per Units
int minCurrent = 350;            // 0.0049V per Units
int maxCurrent = 550;            // 0.0049V per Units
int heatTime = 25000;            // ~500/seconds !!!Attention the time must be enough to let the regulator going to the minimum voltage during the heat time!!!
int highVoltageTime = 1000;       // ~500/seconds. Time starting the high voltage before the regulator begin to stabilize
int stabilizationTime = 7500;    // ~500/seconds. Time required to the regulator to stabilize the current
int switchOffset = 122;	         // 0.0049V per Units (0.6V)
int regAverageCount = 100;       // Number of loop to average the current measures
float indicatorRatio = 0.127;

// Internal use
long reg1CurrentSum = 0;          // Left channel regulator
long reg2CurrentSum = 0;          // Right channel regulator
int regLoopCount = 0;
float reg1CurrentAverage;
float reg2CurrentAverage;
int switchValue = 0;
int heatCount = 0;
int highVoltageCount = 0;
int stabilizationCount = 0;
int ledBlinkCount = 0;
int ledBlinkMax = 0;
int ledBlinkOn = 0;

// Errors
// 3: Current during heat time
// 4: Stabilization too long
// 5: Out of range during normal function
int errorNumber = 100;
int blinkErrorCount = 0;

// Sequence:
// 0: Discharge
// 1: Heat tempo 
// 2: Starting High Voltage
// 3: Waiting for regulator
// 4: Normal Fonction
// 5: Fail
int sequence = 0;
int failStep = 5;

void RelayOn(void) 
{
  analogWrite(relayPin, 128);
}

void RelayOff(void) 
{
  analogWrite(relayPin, 0);
}

void LedOn(void) 
{
  digitalWrite(ledPin, HIGH); 
  digitalWrite(ledOnBoard, HIGH); 
}

void LedOff(void) 
{
  digitalWrite(ledPin, LOW);    
  digitalWrite(ledOnBoard, LOW);    
}

void LedBlinking(void)
{
  ledBlinkCount++;
  if (ledBlinkMax == 0 || ledBlinkCount > ledBlinkMax)
  {
    LedOff();    
    ledBlinkCount = 0;
  }    
  else if (ledBlinkOn == ledBlinkMax)
  {
    LedOn();
    ledBlinkCount = 0;
  }
  else if (ledBlinkCount < ledBlinkOn)
  {
    LedOn();
  }
  else
  {
    LedOff();
  }
}

void BlinkLed(int enabledCount,int totalCount)
{
  ledBlinkMax = totalCount;
  ledBlinkOn = enabledCount;
}

void ResetRegulators(void)
{
  analogWrite(regulatorRefPin, 0); 
}

void DisplayIndicator(void)
{
  int switchValue = analogRead(switchPin) + switchOffset/2;

  if (switchValue < switchOffset)
  {
    // Vu-meter read left channel
    analogWrite(indicatorPin, reg1CurrentAverage * indicatorRatio);     
  }
  else if (switchValue < 2 * switchOffset)
  {
    // Vu-meter is off
    analogWrite(indicatorPin, 0); 
  }
  else
  {
    // Vu-meter read right channel
    analogWrite(indicatorPin, reg2CurrentAverage * indicatorRatio);     
  }
}

boolean CheckInRange(int minValue, int maxValue)
{
  return reg1CurrentAverage > minValue && reg1CurrentAverage < maxValue && reg2CurrentAverage > minValue && reg2CurrentAverage < maxValue;
}

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);     
  pinMode(ledOnBoard, OUTPUT);     
  pinMode(aopFlag1, INPUT);     
  pinMode(aopFlag2, INPUT);     
}

// the loop routine runs over and over again forever:
void loop() 
{ 
  reg1CurrentSum += analogRead(leftCurrentPin);
  reg2CurrentSum += analogRead(rightCurrentPin);
  regLoopCount++;

  if (regLoopCount > regAverageCount)
  {
    reg1CurrentAverage = reg1CurrentSum / regLoopCount;
    reg2CurrentAverage = reg2CurrentSum / regLoopCount;
    regLoopCount = 0;
    reg1CurrentSum = 0;
    reg2CurrentSum = 0;
  }

  LedBlinking();
  DisplayIndicator();

  switch (sequence)
  {  
  case 0:	
    ResetRegulators();
    RelayOff();
    BlinkLed(800, 1000);

    if(regLoopCount != 0 || !CheckInRange(-100, startCurrent))
    {
      break;
    }

    sequence++;

  case 1: 
    if(!CheckInRange(-100, startCurrent + 10))
    {
      // Fail, no current allowed now
      sequence = failStep;
      errorNumber = 3;
      break;
    }
    ResetRegulators();
    RelayOff();
    BlinkLed(400, 800);

    if(heatCount++ < heatTime)
    {
      break;
    }

    sequence++;
    LedOn();
    delay(2500);  

  case 2:
    // Starting High Voltage
    BlinkLed(100, 100);
    RelayOn();

    if(highVoltageCount++ < highVoltageTime)
    {
      break;  
    }  
    
    sequence++;
    
  case 3: 
    // Waiting for regulator
    if(stabilizationCount++ > stabilizationTime)
    {
      // Fail, too late
      sequence = failStep;
      errorNumber = 4;
      break;
    }
    
    BlinkLed(20, 500);
    analogWrite(regulatorRefPin, biasRef); 

    if(!CheckInRange(minCurrent + 10, maxCurrent))
    {
      break;
    }

     sequence++;

  case 4:
    // Normal Fonction, wait and see        
    if(!CheckInRange(minCurrent, maxCurrent))
    {
      // Fail current error
      sequence = failStep;
      errorNumber = 5;
      break;
    }

    BlinkLed(0, 0);
    break;

  default: 
    // Fail, protect mode
    ResetRegulators();
    RelayOff();
    BlinkLed(50, 100);
    
    if (ledBlinkCount == 0)
    {
      blinkErrorCount++;
    }
    
    if (blinkErrorCount > errorNumber)
    {
      blinkErrorCount = 0;
      delay(500);
    }
  }  

  delay(1);  
}









