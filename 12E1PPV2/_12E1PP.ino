/*
  12E1 PP
 Version 2.1.0
 Date: 22.03.2015
 */

#include <SimpleTimer.h>
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <Blink.h>
#include <PID.h>
#include <OneWire.h>
#include <DallasTemperature.h>

static const byte PP12E1_ID = 212;
static const byte ampId = PP12E1_ID;

// Pin Config
#define ledPin                          2
#define reg2Pin		                3
#define screenCurrentPin	        4    
#define oneWireBusPin                   5
#define indicatorPin                    6
#define phaseDetectionPin               7
#define relayPin	                8
#define reg3Pin		                9
#define reg4Pin		                10
#define reg1Pin		                11
#define modulationPin                   A0	    
#define current2Pin	                A1    
#define current4Pin	                A2    
#define current3Pin	                A3    
#define current1Pin	                A4    
#define switchPin	                A5

// Constants
#define startCurrent                      20          // 0.256 per mA
#define minCurrent                        100         // 0.256 per mA
#define maxCurrent                        585         // 0.256 per mA
#define minimalRefCurrent                 176         // 0.256 per mA
#define maximalRefCurrent                 391         // 0.256 per mA
#define dischargeMaxTime	          5   	    // seconds
#define heatMaxTime	                  40  	    // seconds
#define highVoltageMaxTime	          20   	    // seconds
#define regulationMaxTime	          200   	    // seconds
#define functionOutOfRangeMaxTime	  30          // seconds
#define masterP                           0.1      
#define slaveP                            0.2
#define masterI                           0.00007      
#define slaveI                            0.00015   
#define functionMasterI                   0.000005      
#define functionSlaveI                    0.000015
#define pidSampleTime                     100
#define regulationTreshold                4            // 0.256 per mA
#define stabilizedTreshold                60           // 0.256 per mA
#define functionTreshold                  20           // 0.256 per mA
#define currentRatio                      10
#define currentAverageRatio               10 
#define pidSetPointSlaveRatio             3 
#define modulationPercentReductionFactor  0.001
#define heatThinkTempMax                  85          // > 85deg
#define airTempMax                        70          // > 70deg
#define indicatorMaxRange                 180
#define indicatorCorrectionRatio          1.7
#define phaseDetectionErrorMaxTime        100         // loop count
#define screenCurrentErrorMaxTime         300         // milli-seconds

// Internal use
Blink led;
SimpleTimer sendTimer;
SimpleTimer tempMeasureTimer;
SimpleTimer debugTimer;                // Debug
OneWire oneWire(oneWireBusPin);
DallasTemperature tempSensors(&oneWire);
double current1 = 0;
double current2 = 0;
double current3 = 0;
double current4 = 0;
double current1Average = 0;
double current2Average = 0;
double current3Average = 0;
double current4Average = 0;
double modulation = 0;
double modulationPercent = 0;            //Initialized on reset()  
double pid1Output;                       //Initialized on resetRegulators()
double pid2Output;                       //Initialized on resetRegulators() 
double pid3Output;                       //Initialized on resetRegulators() 
double pid4Output;                       //Initialized on resetRegulators() 
unsigned int  percentageSetPoint;        //Initialized on reset()
double pidSetPoint;                      //Initialized on reset()
double pid2SetPoint;                     //Initialized on reset()  
double pid4SetPoint;                     //Initialized on reset()  
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned int  airTemp = 0;
unsigned int  powerSupply1Temp = 0;
unsigned int  powerSupply2Temp = 0;
unsigned long dischargeStartTime;        //Initialized on reset()
unsigned long heatStartTime;             //Initialized on reset()
unsigned long highVoltageStartTime;      //Initialized on reset()
unsigned long regulationStartTime;       //Initialized on reset()
unsigned long functionStartTime;         //Initialized on reset()
unsigned long functionOutOfRangeTime;    //Initialized on reset()
unsigned long phaseDetectionErrorCount = 0; 
unsigned long screenCurrentErrorStartTime = 0; 
unsigned int relayState;                 //Initialized on reset()
unsigned int relayClockState = LOW;

// Debug
//char serialBuffer[12];
//String serialData = "";
//int delimiter = (int) '\n';

// Init regulators
PID pid1(&current1Average, &pid1Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, true);
PID pid2(&current2Average, &pid2Output, &pid2SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, true);
PID pid3(&current3Average, &pid3Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, true);
PID pid4(&current4Average, &pid4Output, &pid4SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, true);

// Timers definition
#define TIMER_CLK_STOP                   0x00       // Timer Stopped
#define TIMER_CLK_DIV1                   0x01       // Timer clocked at F_CPU
#define TIMER_CLK_DIV8                   0x02       // Timer clocked at F_CPU/8
#define TIMER_CLK_DIV64                  0x03       // Timer clocked at F_CPU/64
#define TIMER_CLK_DIV256                 0x04       // Timer clocked at F_CPU/256
#define TIMER_CLK_DIV1024                0x05       // Timer clocked at F_CPU/1024
#define TIMER_PRESCALE_MASK              0x07

// Temp indexes in one wire bus
#define AIR_TEMPERATURE            2
#define POWERSUPPLY1_TEMPERATURE   0
#define POWERSUPPLY2_TEMPERATURE   1 

// Sequence:
#define SEQ_DISCHARGE              0      // 0: Discharge
#define SEQ_HEAT                   1      // 1: Heat tempo 
#define SEQ_STARTING               2      // 2: Starting High Voltage
#define SEQ_REGULATING             3      // 3: Waiting for reg
#define SEQ_FUNCTION               4      // 4: Normal Fonction
#define SEQ_FAIL                   5      // 5: Fail
unsigned int sequence = SEQ_DISCHARGE;

// Errors
#define NO_ERR                       0      // No error
#define ERR_DISHARGETOOLONG          2      // 2: Discharge too long
#define ERR_CURRENTONHEAT            3      // 3: Current during heat time
#define ERR_STARTINGOUTOFRANGE       4      // 4: Out of range during starting
#define ERR_REGULATINGTOOLONG        5      // 5: Regulation too long
#define ERR_REGULATINGMAXREACHED     6      // 6: Maximun reached during regulation
#define ERR_REGULATINGMINREACHED     7      // 7: Minimum reached during regulation
#define ERR_FUNCTIONMAXREACHED       8      // 8: Maximun reached during normal function
#define ERR_FUNCTIONMINREACHED       9      // 9: Minimum reached during normal function
#define ERR_FUNCTIONOUTOFRANGE       10     // 10: Time elapsed with current out of range during normal function
#define ERR_STARTINGTOOLONG          11     // 11: Starting too long
#define ERR_TEMPTOOHIGH              12     // 12: Temperature maximum reached
#define ERR_SCREENCURRENTTOOHIGH     13     // 13: Screen current too high
#define ERR_PHASE                    14     // 14: Phase detction error 
unsigned int errorNumber = NO_ERR;

#define ERR_TUBE_1       1
#define ERR_TUBE_2       2
#define ERR_TUBE_3       3
#define ERR_TUBE_4       4
#define ERR_TUBE_5       5
#define ERR_TUBE_6       6
#define ERR_TUBE_7       7
#define ERR_TUBE_8       8
#define ERR_TEMP_AIR     1
#define ERR_TEMP_PS1     2
#define ERR_TEMP_PS2     3
unsigned int errorCause = NO_ERR;
boolean displayTubeNumber = false;

#define CHECK_RANGE_OK       0
#define CHECK_RANGE_TOOLOW   1
#define CHECK_RANGE_TOOHIGH  2

#define WATCHDOG_DELAY_16MS    0
#define WATCHDOG_DELAY_32MS    1
#define WATCHDOG_DELAY_64MS    2
#define WATCHDOG_DELAY_128MS   3
#define WATCHDOG_DELAY_250MS   4
#define WATCHDOG_DELAY_500MS   5
#define WATCHDOG_DELAY_1S      6
#define WATCHDOG_DELAY_2S      7
#define WATCHDOG_DELAY_4S      8
#define WATCHDOG_DELAY_8S      9

// Diagnostic
EasyTransfer dataTx; 
dataResponse dataTxStruct;

void reset(){
  dischargeStartTime = 0;
  heatStartTime = 0;
  highVoltageStartTime = 0;
  regulationStartTime = 0;
  functionOutOfRangeTime = 0;
  functionStartTime = 0;
  modulationPercent = 0;
  percentageSetPoint = 0;
  pidSetPoint = minimalRefCurrent;
  pid2SetPoint = 0;
  pid4SetPoint = 0;
  resetRegulators();
}

void resetRegulators(){
  relayState = false;

  pid1.SetEnabled(false);  
  pid2.SetEnabled(false);  
  pid3.SetEnabled(false);  
  pid4.SetEnabled(false);  

  pid1Output = 255;
  pid2Output = 255;
  pid3Output = 255;
  pid4Output = 255;

  analogWrite(reg1Pin, 255);
  analogWrite(reg2Pin, 255);
  analogWrite(reg3Pin, 255);
  analogWrite(reg4Pin, 255);
}

void initRegulators()
{
  if (sequence == SEQ_FUNCTION){
    pid1.SetGains(0, functionMasterI, 0);
    pid2.SetGains(0, functionSlaveI, 0);
    pid3.SetGains(0, functionMasterI, 0);
    pid4.SetGains(0, functionSlaveI, 0);    
  }
  else{
    pid1.SetGains(masterP, masterI, 0);
    pid2.SetGains(slaveP, slaveI, 0);
    pid3.SetGains(masterP, masterI, 0);
    pid4.SetGains(slaveP, slaveI, 0);    
  }

  pid1.SetEnabled(true);
  pid2.SetEnabled(true);
  pid3.SetEnabled(true);
  pid4.SetEnabled(true);
}

void computeRegulators()
{
  if (pid1.Compute()) {
    analogWrite(reg1Pin, constrain((int)pid1Output, 0, 255));
  }
  if (pid2.Compute()) {
    analogWrite(reg2Pin, constrain((int)pid2Output, 0, 255));
  }
  if (pid3.Compute()) {
    analogWrite(reg3Pin, constrain((int)pid3Output, 0, 255));
  }
  if (pid4.Compute()) {
    analogWrite(reg4Pin, constrain((int)pid4Output, 0, 255));
  }
}

unsigned int checkInRange(double minValue, double maxValue)
{
  if (minValue > 0){
    if (current1Average < minValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOLOW;
    } 
    if (current2Average < minValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOLOW;
    }
    if (current3Average < minValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOLOW;
    }
    if (current4Average < minValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOLOW;
    }
  }

  if (maxValue > 0){
    if (current1Average > maxValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOHIGH;
    } 
    if (current2Average > maxValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current3Average > maxValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current4Average > maxValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOHIGH;
    }
  }

  errorCause = NO_ERR;
  return CHECK_RANGE_OK;
}

unsigned int calcRegulationProgress(double minValue, double maxValue, double range)
{
  double percentProgress = 100;

  if (current1Average < minValue)
  {
    percentProgress = 100 * (1 - (minValue - current1Average) / range);
  }
  else if (current1Average > maxValue)
  {
    percentProgress = 100 * (1 - (current1Average - maxValue) / range);
  }

  if (current2Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current2Average) / range), percentProgress);
  }
  else if (current2Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current2Average - maxValue) / range), percentProgress);
  }

  if (current3Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current3Average) / range), percentProgress);
  }
  else if (current3Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current3Average - maxValue) / range), percentProgress);
  }

  if (current4Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current4Average) / range), percentProgress);
  }
  else if (current4Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current4Average - maxValue) / range), percentProgress);
  }

  return constrain((int)percentProgress, 0, 100);
}

void sendDatas()
{    
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map(current1Average, 0, 1023, 0, 255);   // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.measure1 = map(current2Average, 0, 1023, 0, 255);
  dataTxStruct.measure2 = map(current3Average, 0, 1023, 0, 255); 
  dataTxStruct.measure3 = map(current4Average, 0, 1023, 0, 255);
  dataTxStruct.measure4 = constrain((int)(modulation * 10), 0, 255);
  dataTxStruct.measure5 = map((int)modulationPercent, 0, 100, 0, 255);
  dataTxStruct.output0 = 255 - constrain((int)pid1Output, 0, 255);
  dataTxStruct.output1 = 255 - constrain((int)pid2Output, 0, 255);
  dataTxStruct.output2 = 255 - constrain((int)pid3Output, 0, 255);
  dataTxStruct.output3 = 255 - constrain((int)pid4Output, 0, 255);
  dataTxStruct.temperature0 = constrain(airTemp, 0, 255); 
  dataTxStruct.temperature1 = constrain(powerSupply1Temp, 0, 255); 
  dataTxStruct.temperature2 = constrain(powerSupply2Temp, 0, 255);   
  dataTxStruct.minValue = map(minCurrent, 0, 1023, 0, 255);
  dataTxStruct.refValue = map(pidSetPoint, 0, 1023, 0, 255); 
  dataTxStruct.maxValue = map(maxCurrent, 0, 1023, 0, 255);  
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorCause;
  dataTx.sendData();

  if (modulationPercent > modulationPercentReductionFactor ){
    modulationPercent -= modulationPercentReductionFactor;
  } 
  else {
    modulationPercent = 0;
  }
}

void measureTemperatures()
{
  // Send the command to get temperatures  
  tempSensors.requestTemperatures(); 
  airTemp = tempSensors.getTempCByIndex(AIR_TEMPERATURE);
  powerSupply1Temp = tempSensors.getTempCByIndex(POWERSUPPLY1_TEMPERATURE);
  powerSupply2Temp = tempSensors.getTempCByIndex(POWERSUPPLY2_TEMPERATURE);
}

void relayOn() 
{
  relayState = true;
}

void relayOff() 
{
  relayState = false;
}

void regulate(){
  // Reset all elapsed time and force regulation
  functionStartTime = 0;
  regulationStartTime = 0;
  sequence = SEQ_REGULATING;
}

void displayIndicator()
{
  displayTubeNumber = false;
  unsigned int value = 0;
  // Limits 0,56,180,316,432
  int switchValue = analogRead(switchPin);  
  if (switchValue < 30)
  {
    // Vu-meter read working left position 1
    value = current1Average; 
  }
  else if (switchValue < 100)
  {
    // Vu-meter read working left position 2
    value = current2Average;     
  }
  else if (switchValue < 270)
  {
    // Vu-meter is off
    value = 0;    
    displayTubeNumber = true;
  }
  else if (380)
  {
    // Vu-meter read working left position 3
    value = current3Average;     
  }
  else
  {
    // Vu-meter read working left position 4
    value = current4Average;    
  }

  double displayValue = value * indicatorCorrectionRatio;
  analogWrite(indicatorPin, map(constrain((int)displayValue, 0, 1023), 0, 1023, 0, indicatorMaxRange));     

}

unsigned int calcPercentSetPoint(){  
  if (modulationPercent > 75) {
    return 100;
  } 
  if (modulationPercent > 50) {
    return 75;
  } 
  if (modulationPercent > 25) {
    return 50;
  } 
  if (modulationPercent > 0) {
    return 25;
  } 
  return 0;
}

void debugFn(){
  // Debug
  /*Serial.print("modulation: ");
   Serial.println(analogRead(modulationPin));
   Serial.print("currentMax: ");
   Serial.println(currentMax);
   
  /*while (Serial.available()) {
   int ch = Serial.read();
   if (ch == -1) {
   }
   else if (ch == delimiter) {
   break;
   }
   else {
   serialData += (char) ch;
   }
   }
   
   int serialLength = serialData.length() + 1;
   if (serialLength > 1){
   serialData.toCharArray(serialBuffer, serialLength);
   
   serialData = "";
   
   unsigned int percentage = atoi(serialBuffer);
   
   pidSetPoint = percentage * (maximalRefCurrent - minimalRefCurrent) / 100 + minimalRefCurrent; 
   regulate();
   
   Serial.print("Set point set to: ");    
   Serial.println(pidSetPoint);
   }*/
}

void setupWatchdog(unsigned int timeLaps) {
  byte bb;
  if (timeLaps > 9 ){
    timeLaps = 9;
  } 
  bb = timeLaps & 7;
  if (timeLaps > 7){
    bb |= (1 << 5);
  }
  bb |= (1 << WDCE);

  MCUSR &= ~(1 << WDRF);
  // start timed sequence
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  // set new watchdog timeout value
  WDTCSR = bb;
  WDTCSR |= _BV(WDIE);
}

// Watchdog Interrupt Service / is executed when  watchdog timed out
ISR(WDT_vect) {
  if (relayState) {
    relayClockState = relayClockState == HIGH ? LOW : HIGH;
    digitalWrite(relayPin, relayClockState);
  }    
}

void setup() {      
  Serial.begin(9600);

  // initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);     
  pinMode(relayPin, OUTPUT);  
  pinMode(oneWireBusPin, OUTPUT);    
  pinMode(phaseDetectionPin, INPUT_PULLUP);
  pinMode(screenCurrentPin, INPUT); 

  // Set pwm speed 
  TCCR1B = (TCCR1B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR2B = (TCCR2B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;

  reset();

  setupWatchdog(WATCHDOG_DELAY_16MS);

  digitalWrite(oneWireBusPin, HIGH);

  led.Setup(ledPin, false);
  sendTimer.setInterval(200, sendDatas);
  debugTimer.setInterval(1000, debugFn);

  tempSensors.begin();   
  tempMeasureTimer.setInterval(30000, measureTemperatures);
  tempSensors.requestTemperatures(); 
  measureTemperatures();

  // Diagnostic
  Serial.begin(9600);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);

  analogReference(INTERNAL);
}

// the loop routine runs over and over again forever:
void loop() 
{ 
  unsigned int currentTime;
  unsigned int check;

  //checkPhase
  if (digitalRead(phaseDetectionPin) == HIGH){
    phaseDetectionErrorCount++;
    if (phaseDetectionErrorCount > phaseDetectionErrorMaxTime){
      // Fail, phase error
      sequence = SEQ_FAIL;
      errorNumber = ERR_PHASE;
    }
  }  
  else{
    phaseDetectionErrorCount = 0;
  }

  if (sequence != SEQ_FAIL && sequence >= SEQ_STARTING && digitalRead(screenCurrentPin) == LOW){
    if (screenCurrentErrorStartTime == 0){
      screenCurrentErrorStartTime = millis();
    }    
    else if (millis() - screenCurrentErrorStartTime > screenCurrentErrorMaxTime){
      // Fail, screen current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_SCREENCURRENTTOOHIGH;
    }  
  }
  else{
    screenCurrentErrorStartTime = 0;
  }

  if (sequence != SEQ_FAIL){    
    if (airTemp > airTempMax) {
      // Fail, air temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_AIR;
    }
    else if (powerSupply1Temp > heatThinkTempMax) {
      // Fail, regulators 1 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_PS1;
    }
    else if (powerSupply2Temp > heatThinkTempMax) {
      // Fail, regulators 2 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_PS2;
    }
    else {
      // Read and smooth the input
      current1 += (analogRead(current1Pin)-current1)/currentRatio;
      current2 += (analogRead(current2Pin)-current2)/currentRatio;
      current3 += (analogRead(current3Pin)-current3)/currentRatio;
      current4 += (analogRead(current4Pin)-current4)/currentRatio;

      current1Average += (current1-current1Average)/currentAverageRatio;
      current2Average += (current2-current2Average)/currentAverageRatio;
      current3Average += (current3-current3Average)/currentAverageRatio;
      current4Average += (current4-current4Average)/currentAverageRatio;    

      if (sequence == SEQ_FUNCTION) {
        modulation = max(current3 + current4 - current3Average - current4Average, 0) + max(current1 + current2 - current1Average - current2Average, 0);
        if (modulation > 20){
          modulationPercent = 100;
        }
        else if (modulation > 15){
          modulationPercent = percentageSetPoint + 75;
        }        
        else if (modulation > 10){
          modulationPercent = percentageSetPoint + 50;        
        } 
        else if (modulation > 5) {
          modulationPercent = percentageSetPoint + 25;        
        }
        if (modulationPercent > 100) {
          modulationPercent = 100;
        }
      }

      // Calc regulators set point
      unsigned int percentage = calcPercentSetPoint();
      if (percentageSetPoint != percentage){
        // New set point must be set
        pidSetPoint = percentage * (maximalRefCurrent - minimalRefCurrent) / 100 + minimalRefCurrent;     
        percentageSetPoint = percentage;
        if (sequence >= SEQ_STARTING){
          regulate();
        }
      }

      // Average set points for slave regulators from master measure
      pid2SetPoint += (current1Average-pid2SetPoint)/pidSetPointSlaveRatio; 
      pid4SetPoint += (current3Average-pid4SetPoint)/pidSetPointSlaveRatio; 

      displayIndicator();
    }
  }

  // Diagnostic
  sendTimer.run();

  // Debug
  //debugTimer.run();

  switch (sequence)
  {  
  case SEQ_DISCHARGE:	
    // Discharging

    // Reset errors
    errorCause = NO_ERR;
    errorNumber = NO_ERR;

    // Pre-sequence
    if (dischargeStartTime == 0){
      dischargeStartTime = millis();
      resetRegulators();
    }

    led.Execute(800, 200);

    // Calc elapsed time in seconds
    currentTime = (millis() - dischargeStartTime) / 1000;

    // Diagnostic
    stepMaxTime = dischargeMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = 0;
    stepCurValue = 1;

    if(currentTime > dischargeMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_DISHARGETOOLONG;
      break;
    }

    if(checkInRange(0, startCurrent) == CHECK_RANGE_TOOHIGH)
    {
      break;
    }

    // Post-sequence
    dischargeStartTime = 0;
    sequence++;

  case SEQ_HEAT: 
    // Startup tempo 

    // Pre-sequence
    if (heatStartTime == 0){
      heatStartTime = millis();
      resetRegulators();
    }

    led.Execute(400, 400);

    // Calc elapsed time in seconds
    currentTime = (millis() - heatStartTime) / 1000;

    // Diagnostic
    stepMaxTime = heatMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = currentTime;

    // Ensure no current at this step
    if(checkInRange(0, startCurrent + 10) == CHECK_RANGE_TOOHIGH)
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_CURRENTONHEAT;
      break;
    }

    if(currentTime < heatMaxTime)
    {
      break;
    }

    // Diagnostic, force 100%
    stepElapsedTime = heatMaxTime;
    stepCurValue = heatMaxTime;

    // Post-sequence
    heatStartTime = 0;
    led.On();
    measureTemperatures();
    sequence++;
    delay(500);   

  case SEQ_STARTING:
    // Starting High Voltage

    // Pre-sequence
    if (highVoltageStartTime == 0){
      highVoltageStartTime = millis();
      relayOn();
      initRegulators();
    }

    // Regulation
    computeRegulators(); 

    led.Execute(20, 400);  

    // Calc elapsed time in seconds
    currentTime = (millis() - highVoltageStartTime) / 1000;

    // Diagnostic
    stepMaxTime = highVoltageMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(pidSetPoint/2, pidSetPoint + stabilizedTreshold, pidSetPoint - stabilizedTreshold); 

    if(currentTime > highVoltageMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGOUTOFRANGE;
      break;      
    }

    // If target points not reached, continue to regulate
    if(checkInRange(pidSetPoint/2, pidSetPoint + stabilizedTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    highVoltageStartTime = 0;
    sequence++;

  case SEQ_REGULATING: 
    // Waiting for reg       

    // Pre-sequence
    if (regulationStartTime == 0){
      regulationStartTime = millis();      
      relayOn();
      initRegulators();
    }

    // Regulation
    computeRegulators(); 

    led.Execute(20, 1500);    

    // Calc elapsed time in seconds
    currentTime = (millis() - regulationStartTime) / 1000;

    // Diagnostic
    stepMaxTime = regulationMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold, pidSetPoint - regulationTreshold); 

    if(currentTime > regulationMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGMAXREACHED;
      break;      
    }

    // If target points not reached, continue to regulate
    if(checkInRange(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    regulationStartTime = 0;
    sequence++;

  case SEQ_FUNCTION:
    // Normal Fonction    

    // Pre-sequence
    if (functionStartTime == 0){
      functionStartTime = millis();
      relayOn();
      initRegulators();
      led.Off();
    }

    // Regulation
    computeRegulators(); 

    // Calc elapsed time in minuts
    currentTime = (millis() - functionStartTime) / 60000; 

    // Diagnostic
    tempMeasureTimer.run();
    stepMaxTime = 0;
    stepElapsedTime = currentTime;
    stepMaxValue = 0;
    stepCurValue = 0;

    check = checkInRange(minCurrent, maxCurrent);
    if(check != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_FUNCTIONMINREACHED : ERR_FUNCTIONMAXREACHED;
      break;
    }

    if(checkInRange(pidSetPoint - functionTreshold, pidSetPoint + functionTreshold) != CHECK_RANGE_OK)
    {
      if (functionOutOfRangeTime == 0){
        functionOutOfRangeTime = millis();
      }

      if (millis() - functionOutOfRangeTime > functionOutOfRangeMaxTime * 1000)
      {    
        // Fail out of range error
        sequence = SEQ_FAIL;
        errorNumber = ERR_FUNCTIONOUTOFRANGE;
        break;
      }          
    }
    else{
      functionOutOfRangeTime = 0;
    }
    break;

  default: 
    // Fail, protect mode
    relayOff();

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;

    // Error indicator
    if (errorNumber == ERR_PHASE){
      // Fast blinking on phase error (Stress blinking)
      led.Execute(80, 80);
    } 
    else {
      // Otherwise display error number or tube number
      led.Execute(250, displayTubeNumber ? errorCause : errorNumber, 1200);
    }
  }  
}







