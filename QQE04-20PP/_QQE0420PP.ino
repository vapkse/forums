/*
QQE04/20 PP
 Version 1.01
 */

// Pin Config
#define ledOnBoard		4
#define ledPin		        13
#define relayPin		3
#define reg1Pin		        9
#define reg2Pin		        10
#define current1Pin	        A0    
#define current2Pin	        A1    

// Constants
#define startCurrent             30        
#define regulatingCurrent        140         
#define minCurrent               370        
#define maxCurrent               750        
#define nominalCurrent           498        
#define heatTime                 15000      // Heat time (40s)
#define startingTime             4000       // Time required to the reg to start the current (10s)
#define stabilizationTime        30000      // Time required to the reg to stabilize the current
#define regulationTime           40000      // Time required to the reg to regulate the current
#define switchOffset		 122        // 0.0049V per Units (0.6V)
#define startingOutput           102        // Starting offset before regulating
#define currentRatioIndicator	 0.25
#define regAbsoluteLoopGain      0.004
#define regRelativeLoopGain      0.006
#define regAbsoluteAverageCount  200
#define regRelativeAverageCount  100
#define tresholdInStabilization  3
#define tresholdInFunction       15
#define tresholdAtStartup        30
#define failMax                  10

// Internal use
int treshold = tresholdAtStartup;
long reg1CurrentSum = 0;
long reg2CurrentSum = 0;
float reg1CurrentAverage = 0;
float reg2CurrentAverage = 0;
int regAbsoluteLoopCount = 0;
int regRelativeLoopCount = 0;
int heatCount = 0;
int startingCount = 0;
int stabilizationCount = 0;
int failCount = 0;
float reg1Output = 0;
float reg2Output = 0;
int ledBlinkCount = 0;
int ledBlinkMax = 0;
int ledBlinkOn = 0;
boolean stabilized = false;

// Errors
#define ERR_2  2      // 2: Current during heat time
#define ERR_3  3      // 3: Out of range during starting
#define ERR_4  4      // 4: Stabilization too long
#define ERR_5  5      // 5: Out of range during stabilization
#define ERR_6  6      // 6: Out of range during normal function
#define ERR_7  7      // 7: Starting too long
#define ERR_8  8      // 8: Regulation too long
#define ERR_9  9      // 9: Too much difference between the 2 regulators working points
#define ERR_10  10    // 10: For test
#define ERR_UNK  100  // Unknown error
int errorNumber = ERR_UNK;
int blinkErrorCount = 0;

// Sequence:
#define SEQ_DISCHARGE    0  // 0: Discharge
#define SEQ_HEAT         1  // 1: Heat tempo 
#define SEQ_STARTING     2  // 2: Starting High Voltage
#define SEQ_REGULATING   3  // 3: Waiting for reg
#define SEQ_FUNCTION     4  // 4: Normal Fonction
#define SEQ_FAIL         5  // 5: Fail
int sequence = SEQ_DISCHARGE;

void RelayOn(void) 
{
  analogWrite(relayPin, 128);
}

void RelayOff(void) 
{
  analogWrite(relayPin, 0);
}

void LedToogle(int state) 
{  
  digitalWrite(ledOnBoard, state); 
  digitalWrite(ledPin, state); 
}

void LedOn()
{
  LedToogle(HIGH);
}

void LedOff() 
{
  LedToogle(LOW);
}

void LedBlinking(void)
{
  ledBlinkCount++;
  if (ledBlinkMax == 0 || ledBlinkCount > ledBlinkMax)
  {
    LedOff();    
    ledBlinkCount = 0;
  }    
  else if (ledBlinkOn == ledBlinkMax)
  {
    LedOn();
    ledBlinkCount = 0;
  }
  else if (ledBlinkCount < ledBlinkOn)
  {
    LedOn();
  }
  else
  {
    LedOff();
  }
}

void BlinkLed(int enabledCount,int totalCount)
{
  ledBlinkMax = totalCount;
  ledBlinkOn = enabledCount;
}

void Reset(void)
{
  SetRegulatorsOutput(0);  
  RelayOff();
}

void SetRegulatorsOutput(int value)
{
  reg1Output = value;
  reg2Output = value;
  SetMasterRegulatorsOutput();
  SetSlaveRegulatorsOutput();  
}

void SetMasterRegulatorsOutput(void)
{
  reg1Output = constrain(reg1Output, 0, 255);
  analogWrite(reg1Pin, reg1Output);
}

void SetSlaveRegulatorsOutput(void)
{
  reg2Output = constrain(reg2Output, 0, 255);
  analogWrite(reg2Pin, reg2Output);
}

boolean CheckInRange(int minValue, int maxValue)
{
  return reg1CurrentAverage >= minValue && reg1CurrentAverage <= maxValue && reg2CurrentAverage >= minValue && reg2CurrentAverage <= maxValue;
}

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);     
  pinMode(ledOnBoard, OUTPUT);     
}

// the loop routine runs over and over again forever:
void loop() 
{ 
  reg1CurrentSum += analogRead(current1Pin);
  reg2CurrentSum += analogRead(current2Pin);
  regAbsoluteLoopCount++;
  regRelativeLoopCount++;

  if (regAbsoluteLoopCount > regAbsoluteAverageCount)
  {
    reg1CurrentAverage = reg1CurrentSum / regAbsoluteLoopCount;
    reg1CurrentSum = 0;
    regAbsoluteLoopCount = 0;
  }

  if (regRelativeLoopCount > regRelativeAverageCount)
  {
    reg2CurrentAverage = reg2CurrentSum / regRelativeLoopCount;
    reg2CurrentSum = 0;
    regRelativeLoopCount = 0;
  }

  LedBlinking();

  switch (sequence)
  {  
  case SEQ_DISCHARGE:	
    // Discharging
    Reset();
    BlinkLed(800, 1000);

    if(regAbsoluteLoopCount != 0 || !CheckInRange(-100, startCurrent))
    {
      break;
    }

    sequence++;

  case SEQ_HEAT: 
    SetRegulatorsOutput(startingOutput);  
    RelayOff();

    // Startup tempo 
    if(!CheckInRange(-100, startCurrent + 10))
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_2;
      break;
    }

    BlinkLed(400, 800);

    if(heatCount++ < heatTime)
    {
      break;
    }

    LedOn();   
    delay(2000);  
    sequence++;

  case SEQ_STARTING:
    // Starting High Voltage
    RelayOn();   
    SetRegulatorsOutput(startingOutput);    
    BlinkLed(20, 200);

    if (!CheckInRange(-100, maxCurrent))
    {
      sequence = SEQ_FAIL;
      errorNumber = ERR_3;
      break;      
    }

    if(startingCount++ > startingTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_7;
      break;
    }

    if(!CheckInRange(regulatingCurrent, maxCurrent))
    {
      break;
    }

    sequence++;

  case SEQ_REGULATING: 
    // Waiting for reg    
    if (regAbsoluteLoopCount == 0)
    {
      reg1Output += regAbsoluteLoopGain * (nominalCurrent - reg1CurrentAverage);
      SetMasterRegulatorsOutput();
    }

    if (regRelativeLoopCount == 0)
    {
      reg2Output += regRelativeLoopGain * (reg1CurrentAverage - reg2CurrentAverage);
      SetSlaveRegulatorsOutput();
    }            

    BlinkLed(20, !stabilized ? 600 : 1200);

    if(stabilized)
    {
      // Check regulation time
      if(stabilizationCount++ > regulationTime)
      {
        // Fail, too late
        sequence = SEQ_FAIL;
        errorNumber = ERR_8;
        break;
      }      

      // Check differences between regultaion working points
      if (abs(reg1Output - reg2Output) > 64)
      {
        sequence = SEQ_FAIL;
        errorNumber = ERR_9;
        break;        
      } 
    }
    else if(stabilizationCount++ > stabilizationTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_4;
      break;
    }


    if (!CheckInRange(stabilized ? minCurrent : regulatingCurrent, maxCurrent))
    {
      if (failCount++ > failMax)
      {
        // Fail current error
        sequence = SEQ_FAIL;
        errorNumber = ERR_5;
        break;
      }      
    }
    else
    {
      failCount = 0;
    }

    if(!CheckInRange(nominalCurrent - treshold, nominalCurrent + treshold))
    {
      break;
    }

    if (!stabilized)
    {
      // Affine without a longer dead time       
      treshold = tresholdInStabilization;
      stabilized = true;    
      break;    
    }

    sequence++;

  case SEQ_FUNCTION:
    // Normal Fonction, wait and see        
    BlinkLed(0, 0);
    RelayOn();

    if(!CheckInRange(minCurrent, maxCurrent))
    {
      if (failCount++ > failMax)
      {
        // Fail min current error
        sequence = SEQ_FAIL;
        errorNumber = ERR_6;
        break;      
      }
    }
    else
    {
      failCount = 0;
    }

    if(!CheckInRange(nominalCurrent - tresholdInFunction, nominalCurrent + tresholdInFunction))
    {
      stabilizationCount++;
    }
    else
    {
      stabilizationCount = 0;
    }

    // If 3 times fail, regulate again
    if (stabilizationCount > 3)
    {    
      // Regulate again
      stabilizationCount = 0;
      sequence--;
      break;
    }    

    break;

  default: 
    // Fail, protect mode
    Reset();
    BlinkLed(100, 250);

    if (ledBlinkCount == 0)
    {
      blinkErrorCount++;
    }

    if (blinkErrorCount >= errorNumber)
    {
      blinkErrorCount = 0;
      delay(1000);
    }
  }  

  delay(2);
}













