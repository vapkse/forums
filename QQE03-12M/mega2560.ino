#include <IRremote.h>
#include <EEPROM.h>
#include <SPI.h>

const boolean debug = false;

extern void *__brkval;
extern int __bss_end;

const int leftVuMeterPin = 6;
const int rightVuMeterPin = 7;
const int IRPin = 11;
const int lcd1Pin = 37;
const int lcd2Pin = 36;
const int lcd3Pin = 35;
const int ledPin = 13;
const int lcdAPin = 22;
const int lcdBPin = 23;
const int lcdCPin = 24;
const int lcdDPin = 25;
const int lcdEPin = 26;
const int lcdFPin = 27;
const int lcdGPin = 28;
const int lcdDPPin = 29;
const int modeButtonPin = 18; // Interrupt
const int displayButtonPin = 19; // Interrupt
const int volPlusButtonPin = 20; // Interrupt
const int volMinusButtonPin = 21; // Interrupt
const int spare0Pin = 40;
const int spare1Pin = 42;
const int spare2Pin = 44;
const int cdiffRelayPin = 47;
const int rsckRelayPin = 46;
const int rsckVolumePin = 48;
const int resetPin = 49; // Send reset to the cards controller
const int mosiPin = 51;
const int sckPin = 52;
const int leftModPin = A7;
const int rightModPin = A6;
// Display modes
const int displayOff = 0;
const int displayInput = 1;
const int displayOutput = 2;
const int displayCdiff = 3;
const int displayReset = 4;
const int displayMax = 4;
// Modes
// For input mode is entry number -1
const int displayOutputLog = 0; // Vu-meters in log mode
const int displayOutputLin = 1; // Vu-meters in lin mode
const int displayCdiffOn = 0; // Cdiff is on
const int displayCdiffOff = 1; // Cdiff is off
const int displayResetNo = 0;
const int displayResetYes = 1;
// Vu meters
const int modPeakReductionFactor = 2;
const int modAverageRatio = 3;
const int vuMeterAverageRatio = 5;
double leftModAverage = 0;
double rightModAverage = 0;
double leftModPeak = 0;
double rightModPeak = 0;
int leftVuMeter = 0;
int rightVuMeter = 0;

// Buttons
const int minPressedTime = 35;
unsigned long modeButtonChangeTime = 0; // Last change time
unsigned long modeButtonState = HIGH; // Last state
unsigned long displayButtonChangeTime = 0; // Last change time
unsigned long displayButtonState = HIGH; // Last state
unsigned long volPlusButtonChangeTime = 0; // Last change time
unsigned long volPlusButtonState = HIGH; // Last state
unsigned long volMinusButtonChangeTime = 0; // Last change time
unsigned long volMinusButtonState = HIGH; // Last state

int volMinusButtonCount = 0;
int volPlusButtonCount = 0;
int displayButtonCount = 0;
int modeButtonCount = 0;
const int buttonPressedMinTime = 3;
const int volRepeatFirstTime = 400;
const int volRepeatNextTime = 65;
int volRepeatTime = volRepeatFirstTime;

// Display
int displayValue = 0;
int modeValue[displayMax + 1];
byte lights[3];
int currentDigit = 0;

int tempDisplayValue = 0;
unsigned long irReceiveTime = 0;
const int tempDelay = 3000;

// Reset
unsigned long resetStartTime = 0;

// IR
IRrecv irrecv(IRPin);
decode_results results;
unsigned long irvalue;
unsigned long irTempTime = 0;

// Eeprom
const int writeEepromDelay = 30000;
unsigned long writeEepromTime = 0;

// Volume
int volume = 0;
unsigned long volumeDisplayTempTime = 0;

// Shift register
const int writeSRDelay = 500;
unsigned long writeSRTime = 0;

int getFreeMemory()
{
  int free_memory;

  if ((int)__brkval == 0)
    free_memory = ((int)&free_memory) - ((int)&__bss_end);
  else
    free_memory = ((int)&free_memory) - ((int)__brkval);

  return free_memory;
}

void setup() {
  pinMode(volMinusButtonPin, INPUT_PULLUP);
  pinMode(volPlusButtonPin, INPUT_PULLUP);
  pinMode(displayButtonPin, INPUT_PULLUP);
  pinMode(modeButtonPin, INPUT_PULLUP);
  pinMode(IRPin, INPUT_PULLUP);

  //Set Pin Modes as outputs
  DDRA = B11111111;
  // DDRB = B11111111; // SPI
  DDRC = B11111111;
  DDRG = B11111111;

  //Set Pin Modes as outputs excepts reset
  DDRL = B01111111;

  //Turn Everything Off
  PORTA = B11111111; // Set all segment pins off.  High for common anode
  //PORTB = B00000000; // SPI
  PORTC = B00000000; // Set all digit pins off.  Low for common anode
  PORTG = B00000000; // Set all output to low
  PORTL = B00000000; // Set all output to low

  // Setup SPI for shift registers
  SPI.begin();

  // Setup IR
  irrecv.enableIRIn(); // Start the receiver

  // Open serial communications and wait for port to open:
  Serial.begin(115200);

  if (debug) {
    Serial.println("Setup timers!");
  }

  // initialize Timer1
  noInterrupts(); // disable all interrupts

  attachInterrupt(digitalPinToInterrupt(modeButtonPin), modeButtonChange, CHANGE);
  attachInterrupt(digitalPinToInterrupt(displayButtonPin), displayButtonChange, CHANGE);
  attachInterrupt(digitalPinToInterrupt(volPlusButtonPin), volPlusButtonChange, CHANGE);
  attachInterrupt(digitalPinToInterrupt(volMinusButtonPin), volMinusButtonChange, CHANGE);

  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;
  OCR1A = 6000; // 96ms
  TCCR1B |= (1 << WGM12); // CTC mode
  TCCR1B |= (1 << CS12); // 256 prescaler
  TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt

  TCCR5A = 0;
  TCCR5B = 0;
  TCNT5 = 0;
  OCR5A = 250;
  TCCR5B |= (1 << WGM52); // CTC mode
  TCCR5B |= (1 << CS52); // 256 prescaler
  TIMSK5 |= (1 << OCIE5A); // enable timer compare interrupt

  interrupts(); // enable all interrupts

  // Restore values from eprom
  readEeprom();
  modeValue[displayReset] = displayResetNo;

  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  if (debug) {
    Serial.println("Setup done!");
  }
}

void displayButtonChange() {
  displayButtonChangeTime = millis();
  displayButtonState = digitalRead(displayButtonPin);
}

void modeButtonChange() {
  modeButtonChangeTime = millis();
  modeButtonState = digitalRead(modeButtonPin);
}

void volPlusButtonChange() {
  volPlusButtonChangeTime = millis();
  volPlusButtonState = digitalRead(volPlusButtonPin);
}

void volMinusButtonChange() {
  volMinusButtonChangeTime = millis();
  volMinusButtonState = digitalRead(volMinusButtonPin);
}

ISR(TIMER5_COMPA_vect) // timer compare interrupt service routine
{
  // Turn the relevant digit off
  PORTC = B00000000;

  // Next digit
  currentDigit++;
  if (currentDigit >= 3) {
    currentDigit = 0;
  }

  // Set all segments
  PORTA = lights[currentDigit];

  // Turn the relevant digit on
  PORTC = B00000000 | 1 << currentDigit;

  if (irrecv.decode(&results)) {
    //Serial.println(results.value, HEX);
    irvalue = results.value;
    irrecv.resume(); // Receive the next value
  }
}

ISR(TIMER1_COMPA_vect) // timer compare interrupt service routine
{
  // VU Meters
  int lmod = analogRead(leftModPin);
  int rmod = analogRead(rightModPin);

  if (lmod > 3) {
    lmod = lmod - 3;
  } else {
    lmod = 0;
  }

  if (rmod > 3) {
    rmod = rmod - 3;
  } else {
    rmod = 0;
  }

  if (modeValue[displayOutput] == displayOutputLog) {
    // Log
    leftModAverage += (log10(lmod + 1) * 84.745 - leftModAverage) / modAverageRatio;
    rightModAverage += (log10(rmod + 1) * 84.745 - rightModAverage) / modAverageRatio;
  } else {
    // Lin
    leftModAverage += (map(lmod, 0, 1021, 0, 255) - leftModAverage) / modAverageRatio;
    rightModAverage += (map(lmod, 0, 1021, 0, 255) - rightModAverage) / modAverageRatio;
  }

  if (leftModAverage >= leftModPeak) {
    leftModPeak = leftModAverage;
  } else {
    leftModPeak = leftModPeak / modPeakReductionFactor;
  }

  if (rightModAverage >= rightModPeak) {
    rightModPeak = rightModAverage;
  } else {
    rightModPeak = rightModPeak / modPeakReductionFactor;
  }

  leftVuMeter += (leftModPeak - leftVuMeter) / vuMeterAverageRatio;
  rightVuMeter += (rightModPeak - rightVuMeter) / vuMeterAverageRatio;

  analogWrite(leftVuMeterPin, leftVuMeter);
  analogWrite(rightVuMeterPin, rightVuMeter);
}

void readEeprom() {
  //Serial.println("Read eeprom");
  int sizeOfInt = 2; // 2 byte required to store one value
  int position = 0;
  // Reset mode is not stored on the eeprom (displayMax - 1)
  for (int i = 0; i < displayMax - 1 ; i++) {
    modeValue[i + 1] = EEPROM.read(sizeOfInt * position);
    position++;
  }
  // Read volume
  volume = EEPROM.read(sizeOfInt * position);
  if (volume < 0 || volume > 100) {
    volume = 0;
  }
}

void writeEeprom() {
  //Serial.println("Write eeprom");
  int sizeOfInt = 2; // 2 byte required to store one value
  int position = 0;
  // Reset mode is not stored on the eeprom (displayMax - 1)
  for (int i = 0; i < displayMax - 1 ; i++) {
    EEPROM.update(sizeOfInt * position, modeValue[i + 1]);
    position++;
  }
  // Add volume
  EEPROM.update(sizeOfInt * position, volume);
}

void loop () {
  unsigned long currentTime = millis();
  /*if (getFreeMemory() < 128)
    {
    Serial.println("Not enough memory");
    digitalWrite(ledPin, HIGH);
    delay(5000);
    digitalWrite(ledPin, LOW);
    }*/

  if (irvalue > 0) {
    if (currentTime - irReceiveTime > 200) {
      switch (irvalue) {
        case 0xFFA25D: // armp3 ch-, Display-
        case 0xE0E08679: // Samsung up, Display-
          displayValue--;
          irReceiveTime = currentTime;
          break;

        case 0xFF629D: // armp3 ch, Display Off
        case 0xE0E016E9: // Samsung enter, Display Off
          displayValue = displayOff;
          irReceiveTime = currentTime;
          break;

        case 0xFFE21D: // armp3 ch+, Display+
        case 0xE0E006F9: // Samsung up, Display+
          displayValue++;
          irReceiveTime = currentTime;
          break;

        case 0xFF22DD: // armp3 |<< Mode-
        case 0xE0E0A659: // Samsung >>| Mode-
          modeValue[displayValue]--;
          irReceiveTime = currentTime;
          writeEepromTime = currentTime;
          break;

        case 0xFF02FD: // armp3 >>| Mode+
        case 0xE0E046B9: // Samsung >>| Mode+
          modeValue[displayValue]++;
          irReceiveTime = currentTime;
          writeEepromTime = currentTime;
          break;

        case 0xFF906F: // armp3 eq, toogle cdiff
          tempDisplayValue = displayCdiff;
          modeValue[displayCdiff] = modeValue[displayCdiff] == displayCdiffOn ? displayCdiffOff : displayCdiffOn;
          irTempTime = currentTime;
          irReceiveTime = currentTime;
          writeEepromTime = currentTime;
          break;

        case 0xFF30CF: // armp3 1
          tempDisplayValue = displayInput;
          modeValue[tempDisplayValue] = 0;
          irTempTime = currentTime;
          irReceiveTime = currentTime;
          writeEepromTime = currentTime;
          break;

        case 0xFF18E7: // armp3 2
          tempDisplayValue = displayInput;
          modeValue[tempDisplayValue] = 1;
          irTempTime = currentTime;
          irReceiveTime = currentTime;
          writeEepromTime = currentTime;
          break;

        case 0xFF7A85: // armp3 3
          tempDisplayValue = displayInput;
          modeValue[tempDisplayValue] = 2;
          irTempTime = currentTime;
          irReceiveTime = currentTime;
          writeEepromTime = currentTime;
          break;

        case 0xFF10EF: // armp3 4
          tempDisplayValue = displayInput;
          modeValue[tempDisplayValue] = 3;
          irTempTime = currentTime;
          irReceiveTime = currentTime;
          writeEepromTime = currentTime;
          break;

        case 0xFFE01F: // armp3 Vol-
        case 0xE0E0D02F: // Samsung Vol-
          irReceiveTime = currentTime;
          // TODO
          break;

        case 0xFFA857: // armp3 Vol+
        case 0xE0E0E01F: // Samsung Vol+
          irReceiveTime = currentTime;
          // TODO
          break;

      }
    }
    irvalue = 0;
  }

  // Buttons
  boolean volumeIsDisplayed = volumeDisplayTempTime > 0 && currentTime - volumeDisplayTempTime < tempDelay;
  if (modeButtonChangeTime > 0 && currentTime - modeButtonChangeTime > minPressedTime) {
    if (modeButtonState == LOW) {
      // mode button pressed
      if (volumeIsDisplayed) {
        volumeDisplayTempTime = 0;
      } else {
        modeValue[displayValue]++;
        writeEepromTime = currentTime;
      }
    }
    modeButtonChangeTime = 0;
  }

  if (displayButtonChangeTime > 0 && currentTime - displayButtonChangeTime > minPressedTime) {
    if (displayButtonState == LOW) {
      if (volumeIsDisplayed) {
        volumeDisplayTempTime = 0;
      } else {
        // display button pressed
        displayValue++;
      }
    }
    displayButtonChangeTime = 0;
  }

  if (volPlusButtonState == LOW || volMinusButtonState == LOW) {
    if (volPlusButtonState == LOW) {
      if (currentTime > volPlusButtonChangeTime +  minPressedTime) {
        // volPlus button pressed
        if (volumeIsDisplayed) {
          if (volume < 100) {
            volume += 1;
          }
          writeEepromTime = currentTime;
        }
        volPlusButtonChangeTime = currentTime + volRepeatTime;
        volRepeatTime = volRepeatNextTime;
        volumeDisplayTempTime = currentTime;
      }
    }

    if (volMinusButtonState == LOW) {
      if (currentTime > volMinusButtonChangeTime + minPressedTime) {
        // volMinus button pressed
        if (volumeIsDisplayed) {
          if (volume > 0) {
            volume -= 1;
          }
          writeEepromTime = currentTime;
        }
        volMinusButtonChangeTime = currentTime + volRepeatTime;
        volRepeatTime = volRepeatNextTime;
        volumeDisplayTempTime = currentTime;
      }
    }

    displayDecimal(0, true);
  } else {
    volRepeatTime = volRepeatFirstTime;
    displayDecimal(0, false);
  }

  if (volumeDisplayTempTime > 0 && currentTime - volumeDisplayTempTime < tempDelay) {
    // Display volume from 0 to 99 or Fu (Full)
    if (volume < 100) {
      float fvolume = (float)volume / 10;
      int digit1 = floor(fvolume);
      int digit0 = round(10 * (fvolume - (float)digit1));
      displayAscii(0, 48 + digit0);
      displayAscii(1, 48 + digit1);
      displayAscii(2, 0);
    } else {
      displayAscii(0, 73);
      displayAscii(1, 72);
      displayAscii(2, 0);
   }
  } else if (irTempTime > 0 && currentTime - irTempTime < tempDelay) {
    showDisplay(&tempDisplayValue, &modeValue[tempDisplayValue]);
  } else {
    irTempTime = 0;
    showDisplay(&displayValue, &modeValue[displayValue]);
  }

  // Update eerpom if necessary
  if (currentTime - writeSRTime > writeSRDelay) {
    writeSRTime = currentTime;

    byte relays = 0;
    if (volume < 100) {
      relays += 0x1;
    }

    switch (modeValue[displayInput]) {
      case 0:
        relays += 0x12;
        break;
      case 1:
        relays += 0xA;
        break;
      case 2:
        relays += 0x6;
        break;
    }
    digitalWrite(rsckRelayPin, LOW);
    SPI.transfer(relays);
    digitalWrite(rsckRelayPin, HIGH);

    // Left volume
    digitalWrite(rsckVolumePin, LOW);
    SPI.transfer(0);
    SPI.transfer(volume * 2.55);
    digitalWrite(rsckVolumePin, HIGH);

    // Right volume
    digitalWrite(rsckVolumePin, LOW);
    SPI.transfer(0x10);
    SPI.transfer(volume * 2.55);
    digitalWrite(rsckVolumePin, HIGH);
  }

  digitalWrite(cdiffRelayPin, modeValue[displayCdiff] == displayCdiffOn ? HIGH : LOW);

  // Reset
  if (displayValue != displayReset) {
    modeValue[displayReset] = displayResetNo;
    resetStartTime = 0;
  } else   if (modeValue[displayReset] != displayResetNo) {
    // Reset counter
    if (resetStartTime == 0) {
      resetStartTime = currentTime;
    } else if (currentTime - resetStartTime > 5000) {
      digitalWrite(resetPin, HIGH);
      delay(500);
      digitalWrite(resetPin, LOW);
      modeValue[displayReset] = displayResetNo;
      resetStartTime = 0;
    }
  } else {
    resetStartTime = 0;
  }

  // Update eerpom if necessary
  if (writeEepromTime > 0 && currentTime - writeEepromTime > writeEepromDelay) {
    writeEepromTime = 0;
    writeEeprom();
  }
}

void showDisplay(int *displayVal, int *modeVal) {
  switch (*displayVal) {
    case -1:
      *displayVal = displayCdiff;
      break;

    case displayOff:
      displayAscii(0, 0);
      displayAscii(1, 0);
      displayAscii(2, 0);
      break;

    case displayInput:
      displayAscii(2, 73);
      switch (*modeVal) {
        case -1:
          *modeVal = 3;

        case 0:
          displayAscii(0, 49);
          displayAscii(1, 48);
          break;

        case 1:
          displayAscii(0, 50);
          displayAscii(1, 48);
          break;

        case 2:
          displayAscii(0, 51);
          displayAscii(1, 48);
          break;

        case 3:
          displayAscii(0, 52);
          displayAscii(1, 48);
          break;

        default:
          *modeVal = 0;
          break;
      }
      break;

    case displayOutput:
      displayAscii(2, 100);
      switch (*modeVal) {
        case -1:
          *modeVal = displayOutputLin;

        case displayOutputLog:
          displayAscii(0, 111);
          displayAscii(1, 76);
          break;

        case displayOutputLin:
          displayAscii(0, 110);
          displayAscii(1, 76);
          break;

        default:
          *modeVal = displayOutputLog;
          break;
      }
      break;

    case displayCdiff:
      displayAscii(2, 99);
      switch (*modeVal) {
        case -1:
          *modeVal = displayCdiffOff;

        case displayCdiffOn:
          displayAscii(0, 110);
          displayAscii(1, 111);
          break;

        case displayCdiffOff:
          displayAscii(0, 70);
          displayAscii(1, 111);
          break;

        default:
          *modeVal = displayCdiffOn;
          break;
      }
      break;

    case displayReset:
      displayAscii(2, 114);
      switch (*modeVal) {
        case displayResetNo:
          displayAscii(0, 70);
          displayAscii(1, 111);
          break;

        case displayResetYes:
          if (resetStartTime == 0) {
            displayAscii(0, 53);
          } else {
            unsigned long seconds = (millis() - resetStartTime) / 1000;
            displayAscii(0, 53 - seconds);
          }
          displayAscii(1, 48);
          break;

        default:
          *modeVal = displayResetNo;
          break;
      }
      break;

    default:
      *displayVal = 0;
      break;
  }
}

void displayDecimal(int digit, boolean state) {
  //Set the decimal place lights
  if (state) {
    lights[digit] &= 0B01111111;
  }
  else {
    lights[digit] |= 0B10000000;
  }
}

//From the numbers found, says which LEDs need to be turned on
void displayAscii(int digit, byte ascii) {
  switch (ascii) {
    case 39:
      lights[digit] = 0B11011111;
      break;
    case 45:
      lights[digit] = 0B10111111;
      break;
    case 48:
      lights[digit] = 0B11000000;
      break;
    case 49:
      lights[digit] = 0B11111001;
      break;
    case 50:
      lights[digit] = 0B10100100;
      break;
    case 51:
      lights[digit] = 0B10110000;
      break;
    case 52:
      lights[digit] = 0B10011001;
      break;
    case 53:
      lights[digit] = 0B10010010;
      break;
    case 54:
      lights[digit] = 0B10000010;
      break;
    case 55:
      lights[digit] = 0B11111000;
      break;
    case 56:
      lights[digit] = 0B10000000;
      break;
    case 57:
      lights[digit] = 0B10010000;
      break;
    case 61:
      lights[digit] = 0B10110111;
      break;
    case 65:
      lights[digit] = 0B10001000;
      break;
    case 67:
      lights[digit] = 0B11000110;
      break;
    case 69:
      lights[digit] = 0B10000110;
      break;
    case 70:
      lights[digit] = 0B10001110;
      break;
    case 72:
      lights[digit] = 0B10001001;
      break;
    case 73:
      lights[digit] = 0B11001111;
      break;
    case 74:
      lights[digit] = 0B11100001;
      break;
    case 76:
      lights[digit] = 0B11000111;
      break;
    case 80:
      lights[digit] = 0B10001100;
      break;
    case 85:
      lights[digit] = 0B11000001;
      break;
    case 91:
      lights[digit] = 0B11000110;
      break;
    case 93:
      lights[digit] = 0B11110000;
      break;
    case 95:
      lights[digit] = 0B11110111;
      break;
    case 97:
      lights[digit] = 0B10100000;
      break;
    case 98:
      lights[digit] = 0B10000011;
      break;
    case 99:
      lights[digit] = 0B10100111;
      break;
    case 100:
      lights[digit] = 0B10100001;
      break;
    case 101:
      lights[digit] = 0B10000100;
      break;
    case 102:
      lights[digit] = 0B10001110;
      break;
    case 104:
      lights[digit] = 0B10001011;
      break;
    case 108:
      lights[digit] = 0B11001111;
      break;
    case 110:
      lights[digit] = 0B10101011;
      break;
    case 111:
      lights[digit] = 0B10100011;
      break;
    case 112:
      lights[digit] = 0B10001100;
      break;
    case 113:
      lights[digit] = 0B10011000;
      break;
    case 114:
      lights[digit] = 0B10101111;
      break;
    case 116:
      lights[digit] = 0B10000111;
      break;
    case 117:
      lights[digit] = 0B11100011;
      break;
    default:
      lights[digit] = 0B11111111;
      break;
  }
}

