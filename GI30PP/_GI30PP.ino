/*
GI30 PP
Version 1.13
Date: 02.04.2015
Loaded with Arduino as programmer
*/

#include <SimpleTimer.h>
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <Blink.h>
#include <PID.h>
#include <OneWire.h>
#include <DallasTemperature.h>

static const byte GI30_ID = 30;
static const byte ampId = GI30_ID;

// Pin Config
#define atxPin 2
#define reg4Pin 3
#define relayPin 4
#define oneWireBusPin 5
#define indicatorPin 6
#define buttonPin 7
#define phaseDetectionPin 8
#define reg2Pin 9
#define reg3Pin 10
#define reg1Pin 11
#define ledPin 13
#define current3Pin A0
#define current1Pin A1
#define current2Pin A2
#define current4Pin A3
#define modulationPin A4

// Constants
#define startCurrent 20.0 // 1.01mV per Units, 0.202 per mA
#define maxCurrent 720 // 1.01mV per Units, 0.202 per mA
#define minimalRefCurrent 100.0 // 1.01mV per Units, 0.202 per mA
#define maximalRefCurrent 590.0 // 1.01mV per Units, 0.202 per mA
#define preStandByMaxTime 1200 // Seconds
#define dischargeMinTime 1 // seconds
#define dischargeMaxTime 5 // seconds
#define heatMaxTime 30 // seconds
#define highVoltageMaxTime 10 // seconds
#define regulationMaxTime 60 // seconds
#define outOfRangeMaxTime 300 // seconds
#define buttonPressedStandByTime 3 // seconds
#define buttonPressedMinTime 50 // Milli-seconds
#define buttonPressedMaxTime 800 // Milli-seconds
#define phaseDetectionErrorMaxTime 500 // Milli-seconds
#define masterP 0.1
#define slaveP 0.2
#define masterMinI 0.00015 // At 0% working point
#define masterMaxI 0.000051 // At 100% working point
#define slaveI 0.0002
#define functionMasterI 0.000006
#define functionSlaveI 0.0001
#define pidSampleTime 100
#define regulationTreshold 1 // 1.01mV per Units, 0.202 per mA
#define stabilizedTreshold 60 // 1.01mV per Units, 0.202 per mA
#define functionTreshold 20 // 1.01mV per Units, 0.202 per mA
#define currentAverageRatio 100 // Passé de 20 à 100 le 9.11 (trop sensible à la modulation)
#define pidSetPointSlaveRatio 10
#define modulationPeakAverageRatio 3
#define modulationPeakReductionFactor 0.02
#define heatThinkTempMax 85 // > 85deg
#define airTempMax 70 // > 70deg
#define indicatorDetectStateMaxTime 800 // Milli-seconds
#define indicatorDisplayStateMaxTime 2000 // Milli-seconds
#define currentIndicatorRatio 0.226 // Max = 220
#define referenceIndicatorRatio 0.393 // Max current = maximalCurrent = 560 / Affichage max = 220
#define percentRatioIndicator 2.18
#define diagnosticSendMinTime 200 // Milli-seconds
#define tempMeasureMinTime 3000 // Milli-seconds

// Internal use
Blink led;
unsigned long lastDiagnosticTime = 0;
unsigned long lastTempMeasureTime = 0;
OneWire oneWire(oneWireBusPin);
DallasTemperature tempSensors(&oneWire);
unsigned int minCurrent; //Initialized on reset()
double current1Average = 0;
double current2Average = 0;
double current3Average = 0;
double current4Average = 0;
double modulationPeakAverage = 0;
double modulationCurrentCompensation = 0;
double modulationPeak; //Initialized on reset()
double pid1Output; //Initialized on resetRegulators()
double pid2Output; //Initialized on resetRegulators()
double pid3Output; //Initialized on resetRegulators()
double pid4Output; //Initialized on resetRegulators()
byte percentageSetPoint; //Initialized on reset()
double pidSetPoint; //Initialized on reset()
double pid2SetPoint; //Initialized on reset()
double pid4SetPoint; //Initialized on reset()
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned int airTemp = 0;
unsigned int heatThinkTemp = 0;
unsigned int powerSupply1Temp = 0;
unsigned int powerSupply2Temp = 0;
unsigned long sequenceStartTime; //Initialized on reset()
unsigned long outOfRangeTime; //Initialized on reset()
unsigned long phaseDetectionErrorStartTime = 0;
unsigned int relayState; //Initialized on reset()
unsigned int relayClockState = LOW;
unsigned long buttonPressedStartTime = 0;
unsigned int manualPercentageSetPoint = 20;
unsigned int displayedPercentageSetPoint = 0;
boolean autoSetPoint = true;
boolean heated = false;
boolean ignoreButtonUp = false;

// Init regulators
PID pid1(&current1Average, &pid1Output, &pidSetPoint, masterP, masterMinI, 0, 0, 255, pidSampleTime, false);
PID pid2(&current2Average, &pid2Output, &pid2SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, false);
PID pid3(&current3Average, &pid3Output, &pidSetPoint, masterP, masterMinI, 0, 0, 255, pidSampleTime, false);
PID pid4(&current4Average, &pid4Output, &pid4SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, false);

// Timers definition
#define TIMER_CLK_DIV1 0x01 // Timer clocked at F_CPU
#define TIMER_PRESCALE_MASK 0x07
#define WATCHDOG_DELAY_16MS 0

// Temp indexes in one wire bus
#define AIR_TEMPERATURE 0
#define HEATTHINK_TEMPERATURE 1
#define POWERSUPPLY1_TEMPERATURE 2
#define POWERSUPPLY2_TEMPERATURE 3

// Sequence:
#define SEQ_STANDBY 0 // Stand-by
#define SEQ_PRESTANDBY 1 // Pre-stand-by (before passing to stand-by, HT is off, but heather keep on)
#define SEQ_DISCHARGE 2 // Discharge
#define SEQ_HEAT 3 // Heat tempo
#define SEQ_STARTING 4 // Starting High Voltage
#define SEQ_REGULATING 5 // Waiting for reg
#define SEQ_FUNCTION 6 // Normal Function
#define SEQ_FAIL 7 // Fail
byte sequence = SEQ_DISCHARGE; // Or SEQ_STANBY, up to you

// Indicator states
#define IND_MIN 0
#define IND_NONE 0 // Indicate nothing.
#define IND_CURRENT1 1 // Tube 1 current
#define IND_CURRENT2 2 // Tube 2 current
#define IND_CURRENT3 3 // Tube 3 current
#define IND_CURRENT4 4 // Tube 4 current
#define IND_SETWORKINGPOINT 5 // Working point swiping for manually set
#define IND_MAX 5
byte indicatorState = IND_NONE;
byte indicatorDetectState = IND_NONE;
unsigned long indicatorDisplayStateStartTime = 0;
unsigned long indicatorSetWorkingPointStartTime = 0;
unsigned long indicatorDetectStateStartTime = 0;

// Errors
#define NO_ERR 0
#define ERR_DISHARGETOOLONG 2 // 2: Discharge too long
#define ERR_CURRENTONHEAT 3 // 3: Current during heat time
#define ERR_REGULATINGTOOLONG 4 // 4: Regulation too long
#define ERR_REGULATINGMAXREACHED 5 // 5: Maximun reached during regulation
#define ERR_REGULATINGMINREACHED 6 // 6: Minimum reached during regulation
#define ERR_FUNCTIONMAXREACHED 7 // 7: Maximun reached during normal function
#define ERR_FUNCTIONMINREACHED 8 // 8: Minimum reached during normal function
#define ERR_FUNCTIONOUTOFRANGE 9 // 9: Time elapsed with current out of range during normal function
#define ERR_STARTINGTOOLONG 10 // 10: Starting too long
#define ERR_STARTINGOUTOFRANGE 11 // 11: Out of range during starting
#define ERR_TEMPTOOHIGH 12 // 12: Temperature maximum reached
#define ERR_PHASE 13 // 13: Phase detection error
byte errorNumber = NO_ERR;

#define ERR_TUBE_1 1
#define ERR_TUBE_2 2
#define ERR_TUBE_3 3
#define ERR_TUBE_4 4
#define ERR_TEMP_AIR 1
#define ERR_TEMP_REG 2
#define ERR_TEMP_PWR1 3
#define ERR_TEMP_PWR2 4
byte errorCause = NO_ERR;

#define CHECK_RANGE_OK 0
#define CHECK_RANGE_TOOLOW 1
#define CHECK_RANGE_TOOHIGH 2

// Diagnostic
EasyTransfer dataTx;
dataResponse dataTxStruct;

void reset(){
  sequenceStartTime = 0;
  outOfRangeTime = 0;
  modulationPeak = 0;
  percentageSetPoint = 0;
  pidSetPoint = minimalRefCurrent;
  pid2SetPoint = 0;
  pid4SetPoint = 0;
  minCurrent = 0;
  resetRegulators();
}

void resetRegulators(){
  relayState = false;

  pid1.SetEnabled(false);
  pid2.SetEnabled(false);
  pid3.SetEnabled(false);
  pid4.SetEnabled(false);

  pid1Output = 0;
  pid2Output = 0;
  pid3Output = 0;
  pid4Output = 0;

  analogWrite(reg1Pin, 0);
  analogWrite(reg2Pin, 0);
  analogWrite(reg3Pin, 0);
  analogWrite(reg4Pin, 0);
}

void initRegulators()
{
  if (sequence == SEQ_FUNCTION){
    pid1.SetGains(0, functionMasterI, 0);
    pid2.SetGains(0, functionSlaveI, 0);
    pid3.SetGains(0, functionMasterI, 0);
    pid4.SetGains(0, functionSlaveI, 0);
  }
  else{
    double masterI = min(percentageSetPoint * 2, 100) * (masterMaxI - masterMinI) / 100 + masterMinI;
    pid1.SetGains(masterP, masterI, 0);
    pid2.SetGains(slaveP, slaveI, 0);
    pid3.SetGains(masterP, masterI, 0);
    pid4.SetGains(slaveP, slaveI, 0);
  }

  pid1.SetEnabled(true);
  pid2.SetEnabled(true);
  pid3.SetEnabled(true);
  pid4.SetEnabled(true);
}

void computeRegulators()
{
  if (pid1.Compute()) {
    analogWrite(reg1Pin, constrain((int)pid1Output, 0, 255));
  }
  if (pid2.Compute()) {
    analogWrite(reg2Pin, constrain((int)pid2Output, 0, 255));
  }
  if (pid3.Compute()) {
    analogWrite(reg3Pin, constrain((int)pid3Output, 0, 255));
  }
  if (pid4.Compute()) {
    analogWrite(reg4Pin, constrain((int)pid4Output, 0, 255));
  }
}

byte checkInRange(double minValue, double maxValue)
{
  if (minValue > 0){
    if (current1Average < minValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOLOW;
    }
    if (current2Average < minValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOLOW;
    }
    if (current3Average < minValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOLOW;
    }
    if (current4Average < minValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOLOW;
    }
  }

  if (maxValue > 0){
    if (current1Average > maxValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current2Average > maxValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current3Average > maxValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current4Average > maxValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOHIGH;
    }
  }

  errorCause = NO_ERR;
  return CHECK_RANGE_OK;
}

unsigned int calcRegulationProgress(double minValue, double maxValue, double range)
{
  double percentProgress = 100;

  if (current1Average < minValue)
  {
    percentProgress = 100 * (1 - (minValue - current1Average) / range);
  }
  else if (current1Average > maxValue)
  {
    percentProgress = 100 * (1 - (current1Average - maxValue) / range);
  }

  if (current2Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current2Average) / range), percentProgress);
  }
  else if (current2Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current2Average - maxValue) / range), percentProgress);
  }

  if (current3Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current3Average) / range), percentProgress);
  }
  else if (current3Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current3Average - maxValue) / range), percentProgress);
  }

  if (current4Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current4Average) / range), percentProgress);
  }
  else if (current4Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current4Average - maxValue) / range), percentProgress);
  }

  return constrain((int)percentProgress, 0, 100);
}

void sendDatas()
{
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map(current1Average, 0, 1023, 0, 255); // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.measure1 = map(current2Average, 0, 1023, 0, 255);
  dataTxStruct.measure2 = map(current3Average, 0, 1023, 0, 255);
  dataTxStruct.measure3 = map(current4Average, 0, 1023, 0, 255);
  dataTxStruct.measure4 = map(modulationPeak * 2, 0, 1023, 0, 255);
  dataTxStruct.output0 = constrain((int)pid1Output, 0, 255);
  dataTxStruct.output1 = constrain((int)pid2Output, 0, 255);
  dataTxStruct.output2 = constrain((int)pid3Output, 0, 255);
  dataTxStruct.output3 = constrain((int)pid4Output, 0, 255);
  dataTxStruct.temperature0 = constrain(airTemp, 0, 255);
  dataTxStruct.temperature1 = constrain(heatThinkTemp, 0, 255);
  dataTxStruct.temperature2 = constrain(max(powerSupply1Temp, powerSupply2Temp), 0, 255);
  dataTxStruct.minValue = map(minCurrent, 0, 1023, 0, 255);
  dataTxStruct.refValue = map(pidSetPoint, 0, 1023, 0, 255);
  dataTxStruct.maxValue = map(maxCurrent, 0, 1023, 0, 255);
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorCause;
  dataTx.sendData();

  if (modulationPeak > modulationPeakReductionFactor ){
    modulationPeak -= modulationPeakReductionFactor;
  }
  else {
    modulationPeak = 0;
  }

  lastDiagnosticTime = millis();
}

void measureTemperatures()
{
  // Send the command to get temperatures
  tempSensors.requestTemperatures();
  airTemp = tempSensors.getTempCByIndex(AIR_TEMPERATURE);
  heatThinkTemp = tempSensors.getTempCByIndex(HEATTHINK_TEMPERATURE);
  powerSupply1Temp = tempSensors.getTempCByIndex(POWERSUPPLY1_TEMPERATURE);
  powerSupply2Temp = tempSensors.getTempCByIndex(POWERSUPPLY2_TEMPERATURE);
  lastTempMeasureTime = millis();
}

void atxOn()
{
  digitalWrite(atxPin, LOW);
}

void atxOff()
{
  heated = false;
  reset();
  digitalWrite(atxPin, HIGH);
}

void relayOn(){
  relayState = true;
}

void PreStandBy()
{
  sequence = SEQ_PRESTANDBY;
}

void StandBy()
{
  sequence = SEQ_STANDBY;
}

void Restart()
{
  sequence = SEQ_DISCHARGE;
}

void Starting()
{
  sequence = SEQ_STARTING;
}

void regulate(){
  // Reset all elapsed time and force regulation
  sequenceStartTime = 0;
  sequence = SEQ_REGULATING;
}

void calcMinCurrent(){
  minCurrent = pidSetPoint - 30;
}

void checkPushButton()
{
  // Only hard reset to leave from fail
  if (sequence == SEQ_FAIL) {
    return;
  }

  unsigned long now = millis();
  if (digitalRead(buttonPin) == HIGH)
  {
    if (buttonPressedStartTime == 0){
      buttonPressedStartTime = now;
    }

    unsigned long seconds = (now - buttonPressedStartTime) / 1000;
    if (!ignoreButtonUp && seconds > buttonPressedStandByTime){
      if (sequence != SEQ_STANDBY && sequence != SEQ_PRESTANDBY)
      {
        // Pre Stand-by
        PreStandBy();
        ignoreButtonUp = true;
      }
      else if (sequence == SEQ_PRESTANDBY)
      {
        // Stand-by
        StandBy();
        ignoreButtonUp = true;
      }
    }
  }
  else if (ignoreButtonUp) {
    // Button up must be ignored
    ignoreButtonUp = false;
    buttonPressedStartTime = 0;
  }
  else if (buttonPressedStartTime > 0)
  {
    unsigned long milliSeconds = now - buttonPressedStartTime;
    if (milliSeconds >= buttonPressedMinTime && (sequence == SEQ_STANDBY || sequence == SEQ_PRESTANDBY)){
      // Restart
      if (heated){
        Starting();
      }
      else{
        Restart();
      }
    }
    else if (milliSeconds >= buttonPressedMinTime && milliSeconds <= buttonPressedMaxTime){
      // If working point is in selection, set the working point manually with the current displayed value.
      if (indicatorState == IND_SETWORKINGPOINT) {
        manualPercentageSetPoint = displayedPercentageSetPoint;
        autoSetPoint = false;
        indicatorState = IND_CURRENT1;
      }
      else {
        // Increment indicator state
        indicatorDetectState++;
        if (indicatorDetectState > IND_MAX){
          indicatorDetectState = IND_NONE;
        }
      }
      indicatorDetectStateStartTime = now;
    }
    else {
      // Cancel the operation
      indicatorDetectStateStartTime = 0;
      indicatorDetectState = IND_NONE;
    }

    buttonPressedStartTime = 0;
  }
  else if (indicatorDetectStateStartTime > 0 && now - indicatorDetectStateStartTime > indicatorDetectStateMaxTime){
    setIndicatorState(indicatorDetectState);
    indicatorDetectState = IND_NONE;
    indicatorDetectStateStartTime = 0;
  }
}

void setIndicatorState(byte state){
  if (state == IND_SETWORKINGPOINT && !autoSetPoint) {
    autoSetPoint = true;
    indicatorState = IND_CURRENT1;
  }
  else{
    indicatorDisplayStateStartTime = millis();
    indicatorState = state;
  }
}

void displayIndicator()
{
  if (indicatorState == IND_SETWORKINGPOINT) {
    // Set working point manually, move indicator from end to start to let the user click on the desired current
    unsigned long now = millis();
    if (indicatorSetWorkingPointStartTime == 0) {
      indicatorSetWorkingPointStartTime = now;
    }

    // Decrease of 10% per seconds
    unsigned long elapsed = now - indicatorSetWorkingPointStartTime;
    if (elapsed < 500){
      displayedPercentageSetPoint = 100;
    }
    else if (elapsed > 11000){
      // Too late
      indicatorState = IND_NONE;
    }
    else if (elapsed > 10500){
      displayedPercentageSetPoint = 0;
    }
    else{
      displayedPercentageSetPoint = 100 - (elapsed - 500) / 100;
    }

    analogWrite(indicatorPin, displayedPercentageSetPoint * percentRatioIndicator);
  }
  else {
    indicatorSetWorkingPointStartTime = 0;

    if (millis() - indicatorDisplayStateStartTime < indicatorDisplayStateMaxTime)
    {
      analogWrite(indicatorPin, indicatorState * percentRatioIndicator * 25);
    }
    else
    {
      switch (indicatorState)
      {
        case IND_CURRENT1:
        // Vu-meter read working left position 1
        analogWrite(indicatorPin, current1Average * currentIndicatorRatio);
        break;

        case IND_CURRENT2:
        // Vu-meter read working left position 2
        analogWrite(indicatorPin, current2Average * currentIndicatorRatio);
        break;

        case IND_CURRENT3:
        // Vu-meter read working right position 1
        analogWrite(indicatorPin, current3Average * currentIndicatorRatio);
        break;

        case IND_CURRENT4:
        // Vu-meter read working right position 2
        analogWrite(indicatorPin, current4Average * currentIndicatorRatio);
        break;

        default:
        // IND_NONE:
        analogWrite(indicatorPin, 0);
      }
    }
  }
}

byte calcPercentSetPoint(){
  if (modulationPeak > 412) {
    modulationCurrentCompensation = 532;
    return 100;
  }
  
  if (modulationPeak > 280) {
    modulationCurrentCompensation = 460;
    return 75;
  }

  if (modulationPeak > 180) {
    modulationCurrentCompensation = 376;
    return 50;
  }

  if (modulationPeak > 112) {
    modulationCurrentCompensation = 320;
    return 25;
  }

  modulationCurrentCompensation = 240;
  return 0;
}

void setupWatchdog(unsigned int timeLaps) {
  byte bb;
  if (timeLaps > 9 ){
    timeLaps = 9;
  }
  bb = timeLaps & 7;
  if (timeLaps > 7){
    bb |= (1 << 5);
  }
  bb |= (1 << WDCE);

  MCUSR &= ~(1 << WDRF);
  // start timed sequence
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  // set new watchdog timeout value
  WDTCSR = bb;
  WDTCSR |= _BV(WDIE);
}

// Watchdog Interrupt Service / is executed when watchdog timed out
ISR(WDT_vect) {
  if (relayState) {
    relayClockState = relayClockState == HIGH ? LOW : HIGH;
    digitalWrite(relayPin, relayClockState);
  }
}

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pin as an output.
  pinMode(atxPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  pinMode(phaseDetectionPin, INPUT);

  // Set PWM speed
  //TCCR0B = (TCCR0B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV64;
  TCCR1B = (TCCR1B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR2B = (TCCR2B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;

  digitalWrite(oneWireBusPin, HIGH);

  reset();

  setupWatchdog(WATCHDOG_DELAY_16MS);

  led.Setup(ledPin, false);

  tempSensors.begin();
  tempSensors.requestTemperatures();
  measureTemperatures();

  // Diagnostic
  Serial.begin(9600);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);

  analogReference(INTERNAL);
}

// the loop routine runs over and over again forever:
void loop()
{
  unsigned int elapsedTime;
  unsigned int check;
  unsigned long currentTime = millis();

  //checkPhase
  if (sequence != SEQ_FAIL && digitalRead(phaseDetectionPin) == LOW){
    if (sequence < SEQ_STARTING){
      // Fail, phase error
      sequence = SEQ_FAIL;
      errorNumber = ERR_PHASE;
    }
    else if (phaseDetectionErrorStartTime == 0){
      phaseDetectionErrorStartTime = millis();
    }
    else if (millis() - phaseDetectionErrorStartTime > phaseDetectionErrorMaxTime){
      // Fail, phase error
      sequence = SEQ_FAIL;
      errorNumber = ERR_PHASE;
    }
  }
  else{
    phaseDetectionErrorStartTime = 0;
  }

  if (sequence != SEQ_FAIL){
    if (airTemp > airTempMax) {
      // Fail, air temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_AIR;
    }
    else if (heatThinkTemp > heatThinkTempMax) {
      // Fail, regulators 2 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_REG;
    }
    else if (powerSupply1Temp > heatThinkTempMax) {
      // Fail, regulators 1 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_PWR1;
    }
    else if (powerSupply2Temp > heatThinkTempMax) {
      // Fail, regulators 2 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_PWR2;
    }
    else {
      // Read and smooth the input
      unsigned int current1 = analogRead(current1Pin);
      unsigned int current2 = analogRead(current2Pin);
      unsigned int current3 = analogRead(current3Pin);
      unsigned int current4 = analogRead(current4Pin);

      current1Average += (current1-current1Average)/currentAverageRatio;
      current2Average += (current2-current2Average)/currentAverageRatio;
      current3Average += (current3-current3Average)/currentAverageRatio;
      current4Average += (current4-current4Average)/currentAverageRatio;

      if (autoSetPoint && sequence >= SEQ_REGULATING) {
        unsigned int modulation = analogRead(modulationPin);
        modulationPeakAverage += ((modulation > modulationCurrentCompensation ? modulation-modulationCurrentCompensation : 0) -modulationPeakAverage)/modulationPeakAverageRatio;
        if (modulationPeak < modulationPeakAverage)
        {
          sendDatas();
          modulationPeak = modulationPeakAverage;
        }
        else {
          modulationPeakAverage = 0;
          modulationPeak = 0;
        }
      }

      // Calc regulators set point
      byte percentage = autoSetPoint ? calcPercentSetPoint() : manualPercentageSetPoint;
      if (percentageSetPoint != percentage){
        // New set point must be set
        pidSetPoint = percentage * (maximalRefCurrent - minimalRefCurrent) / 100 + minimalRefCurrent;
        percentageSetPoint = percentage;
        if (sequence >= SEQ_STARTING){
          regulate();
        }
      }

      // Average set points for slave regulators from master measure
      pid2SetPoint += (current1Average-pid2SetPoint)/pidSetPointSlaveRatio;
      pid4SetPoint += (current3Average-pid4SetPoint)/pidSetPointSlaveRatio;

      checkPushButton();
      displayIndicator();
    }
  }

  switch (sequence)
  {
    case SEQ_STANDBY:
    atxOff();
    led.Execute(20, 6000);

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;
    break;

    case SEQ_PRESTANDBY:
    // Pre-sequence
    if (sequenceStartTime == 0){
      reset();
      atxOn();
      sequenceStartTime = millis();
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (millis() - sequenceStartTime) / 1000;
    }

    led.Execute(400, 3000);

    // Diagnostic
    stepMaxTime = preStandByMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = preStandByMaxTime;
    stepCurValue = elapsedTime;

    // Post-sequence
    if (elapsedTime > preStandByMaxTime)
    {
      sequenceStartTime = 0;
      sequence = SEQ_STANDBY;
    }
    break;

    case SEQ_DISCHARGE:
    // Discharging

    // Reset errors
    errorCause = NO_ERR;
    errorNumber = NO_ERR;

    // Pre-sequence
    if (sequenceStartTime == 0){
      atxOn();
      resetRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    led.Execute(800, 200);

    // Diagnostic
    stepMaxTime = dischargeMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 0;
    stepCurValue = 1;

    if(elapsedTime > dischargeMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_DISHARGETOOLONG;
      break;
    }

    if(elapsedTime < dischargeMinTime || checkInRange(0, startCurrent) == CHECK_RANGE_TOOHIGH)
    {
      break;
    }

    // Post-sequence
    sendDatas();
    sequenceStartTime = 0;
    sequence++;

    case SEQ_HEAT:
    // Startup tempo

    // Pre-sequence
    if (sequenceStartTime == 0){
      resetRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    led.Execute(400, 400);

    // Diagnostic
    stepMaxTime = heatMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = elapsedTime;

    // Ensure no current at this step
    if(checkInRange(0, startCurrent + 10) == CHECK_RANGE_TOOHIGH)
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_CURRENTONHEAT;
      break;
    }

    if(elapsedTime < heatMaxTime)
    {
      break;
    }

    // Diagnostic, force 100%
    stepElapsedTime = heatMaxTime;
    stepCurValue = heatMaxTime;

    // Post-sequence
    sequenceStartTime = 0;
    heated = true;
    led.On();
    measureTemperatures();
    sendDatas();
    sequence++;
    delay(250);

    case SEQ_STARTING:
    // Starting High Voltage

    // Pre-sequence
    if (sequenceStartTime == 0){
      relayOn();
      initRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    led.Execute(20, 400);

    // Diagnostic
    stepMaxTime = highVoltageMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(pidSetPoint/2, pidSetPoint + stabilizedTreshold, pidSetPoint - stabilizedTreshold);

    if(elapsedTime > highVoltageMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGOUTOFRANGE;
      break;
    }

    // If target points not reached, continue to regulate
    if(elapsedTime < 5 || checkInRange(pidSetPoint/2, pidSetPoint + stabilizedTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    sequenceStartTime = 0;
    sendDatas();
    sequence++;

    case SEQ_REGULATING:
    // Waiting for reg

    // Pre-sequence
    if (sequenceStartTime == 0){
      atxOn();
      relayOn();
      initRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    led.Execute(20, 1500);

    // Diagnostic
    stepMaxTime = regulationMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold, pidSetPoint - regulationTreshold);

    if(elapsedTime > regulationMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGMAXREACHED;
      break;
    }

    // If target points not reached, continue to regulate
    if(checkInRange(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    sendDatas();
    sequenceStartTime = 0;
    calcMinCurrent();
    sequence++;

    case SEQ_FUNCTION:
    // Normal Function

    // Pre-sequence
    if (sequenceStartTime == 0){
      atxOn();
      relayOn();
      initRegulators();
      led.Off();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    // Measure Temp
    if (currentTime - lastTempMeasureTime > tempMeasureMinTime){
      measureTemperatures();
    }

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 0;
    stepCurValue = 0;

    check = checkInRange(minCurrent, maxCurrent);
    if(check != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_FUNCTIONMINREACHED : ERR_FUNCTIONMAXREACHED;
      break;
    }

    if(checkInRange(pidSetPoint - functionTreshold, pidSetPoint + functionTreshold) != CHECK_RANGE_OK)
    {
      if (outOfRangeTime == 0){
        outOfRangeTime = currentTime;
      }

      if (currentTime - outOfRangeTime > outOfRangeMaxTime * 1000)
      {
        // Fail out of range error
        sequence = SEQ_FAIL;
        errorNumber = ERR_FUNCTIONOUTOFRANGE;
        break;
      }
    }
    else{
      outOfRangeTime = 0;
    }
    break;

    default:
    // Fail, protect mode
    // Ensure relay, and atx off
    atxOff();

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;
    lastDiagnosticTime = 0;

    // Error indicator
    if (errorNumber == ERR_PHASE){
      // Fast blinking on phase error (Stress blinking)
      led.Execute(80, 80);
    }
    else {
      // Otherwise display error number or tube number
      led.Execute(250, digitalRead(buttonPin) == HIGH ? errorCause : errorNumber, 1200);
    }
  }

  // Diagnostic
  if (currentTime - lastDiagnosticTime > diagnosticSendMinTime) {
    sendDatas();
  }
}



