/*
QQE03-12 DPP
 Version 2.1.4
 Date: 24.04.2015
 */

#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <Blink.h>
#include <PID.h>
#include <OneWire.h>
#include <DallasTemperature.h>

static const byte QQE0312PP_ID = 128;
static const byte ampId = QQE0312PP_ID;

// Pin Config
#define indicatorPin 2 // 31KHz
#define reg1Pin 3 // 31KHz
#define relayPin 4 // 1KHz
#define reg2Pin 5 // 31KHz
#define reg3Pin 6 // 31KHz
#define reg4Pin 7 // 31KHz
#define reg5Pin 8 // 31KHz
#define reg6Pin 9 // 31KHz
#define reg7Pin 10 // 31KHz
#define reg8Pin 11 // 31KHz
#define oneWireBusSupply 12
#define oneWireBusPin 13 // 1KHZ
#define ledGreenPin 50
#define ledRedPin 52
#define current1Pin A0
#define current2Pin A1
#define current3Pin A2
#define current4Pin A3
#define current5Pin A4
#define current6Pin A5
#define current7Pin A6
#define current8Pin A7
#define switchPin A8
#define rightModulationPin A9
#define leftModulationPin A11

// Constants
#define startCurrent 30 // 12.45/mA
#define minCurrent 200 // 12.45/mA
#define maxCurrent 685 // 12.45/mA
#define minimalRefCurrent 400 // 12.45/mA
#define maximalRefCurrent 500 // 12.45/mA
#define dischargeMinTime 1 // Seconds
#define dischargeMaxTime 5 // Seconds
#define heatMaxTime 40 // Seconds
#define highVoltageMaxTime 5 // Seconds
#define regulationMaxTime 100 // Seconds
#define outOfRangeMaxTime 300 // Seconds
#define errorMaxTime 1000 // Milli-seconds
#define masterP 3
#define masterI 0.0005
#define slaveP 4
#define slaveI 0.0003
#define functionMasterP 0.04
#define functionSlaveP 0.04
#define functionMasterI 0.000005
#define functionSlaveI 0.0001
#define pidSampleTime 100
#define regulationTreshold 1 // 12.45/mA
#define functionTreshold 25 // 12.45/mA
#define currentAverageRatio 100
#define pidSetPointSlaveRatio 10
#define modulationPeakAverageRatio 1
#define modulationPeakReductionFactor 0.02
#define heatThinkTempMax 85 // > 85deg
#define airTempMax 70 // > 70deg
#define switchOffset 122 // 0.0049V per Units (0.6V)
#define currentRatioIndicator 0.25
#define startingMinCurrent 100 // 12.45/mA
#define diagnosticSendMinTime 200 // Milli-seconds
#define tempMeasureMinTime 3000 // Milli-seconds

// Internal use
Blink ledGreen;
Blink ledRed;
unsigned long lastDiagnosticTime = 0;
unsigned long lastTempMeasureTime = 0;
OneWire oneWire(oneWireBusPin);
DallasTemperature tempSensors(&oneWire);
double current1Average = 0;
double current2Average = 0;
double current3Average = 0;
double current4Average = 0;
double current5Average = 0;
double current6Average = 0;
double current7Average = 0;
double current8Average = 0;
double leftModulationPeakAverage = 0;
double rightModulationPeakAverage = 0;
double modulationPeak; //Initialized on reset()
double pid1Output; //Initialized on resetRegulators()
double pid2Output; //Initialized on resetRegulators()
double pid3Output; //Initialized on resetRegulators()
double pid4Output; //Initialized on resetRegulators()
double pid5Output; //Initialized on resetRegulators()
double pid6Output; //Initialized on resetRegulators()
double pid7Output; //Initialized on resetRegulators()
double pid8Output; //Initialized on resetRegulators()
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned int airTemp = 0;
unsigned int heatThink1Temp = 0;
unsigned int heatThink2Temp = 0;
unsigned long sequenceStartTime; //Initialized on reset()
unsigned long outOfRangeTime; //Initialized on reset()
unsigned long errorTime; //Initialized on reset()
byte percentageSetPoint; //Initialized on reset()
double pidSetPoint; //Initialized on reset()
double pid2SetPoint; //Initialized on reset()
double pid4SetPoint; //Initialized on reset()
double pid6SetPoint; //Initialized on reset()
double pid8SetPoint; //Initialized on reset()
unsigned int leftCurrentMax;
unsigned int rightCurrentMax;

// Init regulators
PID pid1(&current1Average, &pid1Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, false);
PID pid2(&current2Average, &pid2Output, &pid2SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, false);
PID pid3(&current3Average, &pid3Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, false);
PID pid4(&current4Average, &pid4Output, &pid4SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, false);
PID pid5(&current5Average, &pid5Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, false);
PID pid6(&current6Average, &pid6Output, &pid6SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, false);
PID pid7(&current7Average, &pid7Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, false);
PID pid8(&current8Average, &pid8Output, &pid8SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, false);

// Timers definition
#define TIMER_CLK_DIV1 0x01 // Timer clocked at F_CPU
#define TIMER_PRESCALE_MASK 0x07

// Temp indexes in one wire bus
#define AIR_TEMPERATURE 2
#define HEATTHINK1_TEMPERATURE 0
#define HEATTHINK2_TEMPERATURE 1

// Sequence:
#define SEQ_DISCHARGE 0 // 0: Discharge
#define SEQ_HEAT 1 // 1: Heat tempo
#define SEQ_STARTING 2 // 2: Starting High Voltage
#define SEQ_REGULATING 3 // 3: Waiting for reg
#define SEQ_FUNCTION 4 // 4: Normal Function
#define SEQ_FAIL 5 // 5: Fail
byte sequence = SEQ_DISCHARGE;

// Errors
#define NO_ERR 0 // No error
#define ERR_DISHARGETOOLONG 2 // 2: Discharge too long
#define ERR_CURRENTONHEAT 3 // 3: Current during heat time
#define ERR_STARTINGOUTOFRANGE 4 // 4: Out of range during starting
#define ERR_REGULATINGTOOLONG 5 // 5: Regulation too long
#define ERR_REGULATINGMAXREACHED 6 // 6: Maximun reached during regulation
#define ERR_REGULATINGMINREACHED 7 // 7: Minimum reached during regulation
#define ERR_FUNCTIONMAXREACHED 8 // 8: Maximun reached during normal function
#define ERR_FUNCTIONMINREACHED 9 // 9: Minimum reached during normal function
#define ERR_FUNCTIONOUTOFRANGE 10 // 10: Time elapsed with current out of range during normal function
#define ERR_STARTINGTOOLONG 11 // 11: Starting too long
#define ERR_TEMPTOOHIGH 12 // 12: Temperature maximum reached
byte errorNumber = NO_ERR;

#define ERR_TUBE_1 1
#define ERR_TUBE_2 2
#define ERR_TUBE_3 3
#define ERR_TUBE_4 4
#define ERR_TUBE_5 5
#define ERR_TUBE_6 6
#define ERR_TUBE_7 7
#define ERR_TUBE_8 8
#define ERR_TEMP_AIR 1
#define ERR_TEMP_REG1 2
#define ERR_TEMP_REG2 3
byte errorCause = NO_ERR;
boolean displayTubeNumber = false;

#define CHECK_RANGE_OK 0
#define CHECK_RANGE_TOOLOW 1
#define CHECK_RANGE_TOOHIGH 2

// Diagnostic
EasyTransfer dataTx;
dataResponse dataTxStruct;

void reset(){
  sequenceStartTime = 0;
  outOfRangeTime = 0;
  errorTime = 0;
  modulationPeak = 0;
  percentageSetPoint = 0;
  pidSetPoint = minimalRefCurrent;
  pid2SetPoint = 0;
  pid4SetPoint = 0;
  pid6SetPoint = 0;
  pid8SetPoint = 0;
  resetRegulators();
}

void resetRegulators(){
  relayOff();

  pid1.SetEnabled(false);
  pid2.SetEnabled(false);
  pid3.SetEnabled(false);
  pid4.SetEnabled(false);
  pid5.SetEnabled(false);
  pid6.SetEnabled(false);
  pid7.SetEnabled(false);
  pid8.SetEnabled(false);

  pid1Output = 0;
  pid2Output = 0;
  pid3Output = 0;
  pid4Output = 0;
  pid5Output = 0;
  pid6Output = 0;
  pid7Output = 0;
  pid8Output = 0;

  analogWrite(reg1Pin, 0);
  analogWrite(reg2Pin, 0);
  analogWrite(reg3Pin, 0);
  analogWrite(reg4Pin, 0);
  analogWrite(reg5Pin, 0);
  analogWrite(reg6Pin, 0);
  analogWrite(reg7Pin, 0);
  analogWrite(reg8Pin, 0);
}

void initRegulators()
{
  if (sequence == SEQ_FUNCTION){
    pid1.SetGains(functionMasterP, functionMasterI, 0);
    pid2.SetGains(functionSlaveP, functionSlaveI, 0);
    pid3.SetGains(functionMasterP, functionMasterI, 0);
    pid4.SetGains(functionSlaveP, functionSlaveI, 0);
    pid5.SetGains(functionMasterP, functionMasterI, 0);
    pid6.SetGains(functionSlaveP, functionSlaveI, 0);
    pid7.SetGains(functionMasterP, functionMasterI, 0);
    pid8.SetGains(functionSlaveP, functionSlaveI, 0);
  }
  else{
    pid1.SetGains(masterP, masterI, 0);
    pid2.SetGains(slaveP, slaveI, 0);
    pid3.SetGains(masterP, masterI, 0);
    pid4.SetGains(slaveP, slaveI, 0);
    pid5.SetGains(masterP, masterI, 0);
    pid6.SetGains(slaveP, slaveI, 0);
    pid7.SetGains(masterP, masterI, 0);
    pid8.SetGains(slaveP, slaveI, 0);
  }

  pid1.SetEnabled(true);
  pid2.SetEnabled(true);
  pid3.SetEnabled(true);
  pid4.SetEnabled(true);
  pid5.SetEnabled(true);
  pid6.SetEnabled(true);
  pid7.SetEnabled(true);
  pid8.SetEnabled(true);
}

void computeRegulators()
{
  if (pid1.Compute()) {
    analogWrite(reg1Pin, constrain((int)pid1Output, 0, 255));
  }
  if (pid2.Compute()) {
    analogWrite(reg2Pin, constrain((int)pid2Output, 0, 255));
  }
  if (pid3.Compute()) {
    analogWrite(reg3Pin, constrain((int)pid3Output, 0, 255));
  }
  if (pid4.Compute()) {
    analogWrite(reg4Pin, constrain((int)pid4Output, 0, 255));
  }
  if (pid5.Compute()) {
    analogWrite(reg5Pin, constrain((int)pid5Output, 0, 255));
  }
  if (pid6.Compute()) {
    analogWrite(reg6Pin, constrain((int)pid6Output, 0, 255));
  }
  if (pid7.Compute()) {
    analogWrite(reg7Pin, constrain((int)pid7Output, 0, 255));
  }
  if (pid8.Compute()) {
    analogWrite(reg8Pin, constrain((int)pid8Output, 0, 255));
  }
}

byte checkInRange(double minValue, double maxValue)
{
  if (minValue > 0){
    if (current1Average < minValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOLOW;
    }
    if (current2Average < minValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOLOW;
    }
    if (current3Average < minValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOLOW;
    }
    if (current4Average < minValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOLOW;
    }
    if (current5Average < minValue)
    {
      errorCause = ERR_TUBE_5;
      return CHECK_RANGE_TOOLOW;
    }
    if (current6Average < minValue)
    {
      errorCause = ERR_TUBE_6;
      return CHECK_RANGE_TOOLOW;
    }
    if (current7Average < minValue)
    {
      errorCause = ERR_TUBE_7;
      return CHECK_RANGE_TOOLOW;
    }
    if (current8Average < minValue)
    {
      errorCause = ERR_TUBE_8;
      return CHECK_RANGE_TOOLOW;
    }
  }

  if (maxValue > 0){
    if (current1Average > maxValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current2Average > maxValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current3Average > maxValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current4Average > maxValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current5Average > maxValue)
    {
      errorCause = ERR_TUBE_5;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current6Average > maxValue)
    {
      errorCause = ERR_TUBE_6;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current7Average > maxValue)
    {
      errorCause = ERR_TUBE_7;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current8Average > maxValue)
    {
      errorCause = ERR_TUBE_8;
      return CHECK_RANGE_TOOHIGH;
    }
  }

  errorCause = NO_ERR;
  return CHECK_RANGE_OK;
}

unsigned int calcRegulationProgress(double minValue, double maxValue, double range)
{
  double percentProgress = 100;

  if (current1Average < minValue)
  {
    percentProgress = 100 * (1 - (minValue - current1Average) / range);
  }
  else if (maxValue > 0 && current1Average > maxValue)
  {
    percentProgress = 100 * (1 - (current1Average - maxValue) / range);
  }

  if (current2Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current2Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current2Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current2Average - maxValue) / range), percentProgress);
  }

  if (current3Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current3Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current3Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current3Average - maxValue) / range), percentProgress);
  }

  if (current4Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current4Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current4Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current4Average - maxValue) / range), percentProgress);
  }

  if (current5Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current5Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current5Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current5Average - maxValue) / range), percentProgress);
  }

  if (current6Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current6Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current6Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current6Average - maxValue) / range), percentProgress);
  }

  if (current7Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current7Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current7Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current7Average - maxValue) / range), percentProgress);
  }

  if (current8Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current8Average) / range), percentProgress);
  }
  else if (maxValue > 0 && current8Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current8Average - maxValue) / range), percentProgress);
  }

  return constrain((int)percentProgress, 0, 100);
}

void sendDatas()
{
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map(current1Average, 0, 1023, 0, 255); // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.measure1 = map(current2Average, 0, 1023, 0, 255);
  dataTxStruct.measure2 = map(current3Average, 0, 1023, 0, 255);
  dataTxStruct.measure3 = map(current4Average, 0, 1023, 0, 255);
  dataTxStruct.measure4 = map(current5Average, 0, 1023, 0, 255);
  dataTxStruct.measure5 = map(current6Average, 0, 1023, 0, 255);
  dataTxStruct.measure6 = map(current7Average, 0, 1023, 0, 255);
  dataTxStruct.measure7 = map(current8Average, 0, 1023, 0, 255);
  dataTxStruct.output0 = constrain((int)pid1Output, 0, 255);
  dataTxStruct.output1 = constrain((int)pid2Output, 0, 255);
  dataTxStruct.output2 = constrain((int)pid3Output, 0, 255);
  dataTxStruct.output3 = constrain((int)pid4Output, 0, 255);
  dataTxStruct.output4 = constrain((int)pid5Output, 0, 255);
  dataTxStruct.output5 = constrain((int)pid6Output, 0, 255);
  dataTxStruct.output6 = constrain((int)pid7Output, 0, 255);
  dataTxStruct.output7 = constrain((int)pid8Output, 0, 255);
  dataTxStruct.temperature0 = constrain(airTemp, 0, 255);
  dataTxStruct.temperature1 = constrain(heatThink1Temp, 0, 255);
  dataTxStruct.temperature2 = constrain(heatThink2Temp, 0, 255);
  dataTxStruct.temperature3 = map(modulationPeak, 0, 1023, 0, 255);
  dataTxStruct.minValue = map(minCurrent, 0, 1023, 0, 255);
  dataTxStruct.refValue = map(pidSetPoint, 0, 1023, 0, 255);
  dataTxStruct.maxValue = map(maxCurrent, 0, 1023, 0, 255);
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorCause;
  dataTx.sendData();

  if (modulationPeak > modulationPeakReductionFactor ){
    modulationPeak -= modulationPeakReductionFactor;
  }
  else {
    modulationPeak = 0;
  }

  lastDiagnosticTime = millis();
}

void measureTemperatures()
{
  // Send the command to get temperatures
  tempSensors.requestTemperatures();
  airTemp = tempSensors.getTempCByIndex(AIR_TEMPERATURE);
  heatThink1Temp = tempSensors.getTempCByIndex(HEATTHINK1_TEMPERATURE);
  heatThink2Temp = tempSensors.getTempCByIndex(HEATTHINK2_TEMPERATURE);
  lastTempMeasureTime = millis();
}

void relayOn() {
  analogWrite(relayPin, 127);
}

void relayOff() {
  analogWrite(relayPin, 0);
}

void regulate(){
  // Reset all elapsed time and force regulation
  sequenceStartTime = 0;
  sequence = SEQ_REGULATING;
}

void displayIndicator()
{
  displayTubeNumber = false;
  int switchValue = analogRead(switchPin) + switchOffset/2;
  if (switchValue < switchOffset)
  {
    // Vu-meter read working left position 1
    analogWrite(indicatorPin, current1Average * currentRatioIndicator);
  }
  else if (switchValue < 2 * switchOffset)
  {
    // Vu-meter read working left position 2
    analogWrite(indicatorPin, current2Average * currentRatioIndicator);
  }
  else if (switchValue < 3 * switchOffset)
  {
    // Vu-meter read working left position 3
    analogWrite(indicatorPin, current3Average * currentRatioIndicator);
  }
  else if (switchValue < 4 * switchOffset)
  {
    // Vu-meter read working left position 4
    analogWrite(indicatorPin, current4Average * currentRatioIndicator);
  }
  else if (switchValue < 5 * switchOffset)
  {
    analogWrite(indicatorPin, 0);
    displayTubeNumber = true;
  }
  else if (switchValue < 6 * switchOffset)
  {
    // Vu-meter read working right position 1
    analogWrite(indicatorPin, current5Average * currentRatioIndicator);
  }
  else if (switchValue < 7 * switchOffset)
  {
    // Vu-meter read working right position 2
    analogWrite(indicatorPin, current6Average * currentRatioIndicator);
  }
  else if (switchValue < 8 * switchOffset)
  {
    // Vu-meter read working right position 3
    analogWrite(indicatorPin, current7Average * currentRatioIndicator);
  }
  else if (switchValue < 9 * switchOffset)
  {
    // Vu-meter read working right position 4
    analogWrite(indicatorPin, current8Average * currentRatioIndicator);
  }
  else
  {
    // Vu-meter is off
    analogWrite(indicatorPin, 0);
  }
}

byte calcPercentSetPoint(){
  if (modulationPeak > 300) {
    return 100;
  }
  return 0;
}

void setup() {
  Serial.begin(9600);

  // initialize the digital pin as an output.
  pinMode(ledRedPin, OUTPUT);
  pinMode(ledGreenPin, OUTPUT);
  pinMode(oneWireBusSupply, OUTPUT);

  // Set PWM speed to the maximum (32KHZ)
  TCCR1B = (TCCR1B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR2B = (TCCR2B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR3B = (TCCR3B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR4B = (TCCR4B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;

  digitalWrite(oneWireBusSupply, HIGH);
  digitalWrite(oneWireBusPin, HIGH);

  reset();

  ledGreen.Setup(ledGreenPin, true);
  ledRed.Setup(ledRedPin, true);

  tempSensors.begin();
  tempSensors.requestTemperatures();
  measureTemperatures();

  // Diagnostic
  Serial3.begin(9600);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial3);
}

void loop()
{
  unsigned int elapsedTime;
  unsigned int check;
  unsigned long currentTime = millis();

  if (sequence != SEQ_FAIL){
    if (airTemp > airTempMax) {
      // Fail, air temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_AIR;
    }
    else if (heatThink1Temp > heatThinkTempMax) {
      // Fail, regulators 1 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_REG1;
    }
    else if (heatThink2Temp > heatThinkTempMax) {
      // Fail, regulators 2 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_REG2;
    }
    else {
      // Read and smooth the input
      unsigned int current1 = analogRead(current1Pin);
      unsigned int current2 = analogRead(current2Pin);
      unsigned int current3 = analogRead(current3Pin);
      unsigned int current4 = analogRead(current4Pin);
      unsigned int current5 = analogRead(current5Pin);
      unsigned int current6 = analogRead(current6Pin);
      unsigned int current7 = analogRead(current7Pin);
      unsigned int current8 = analogRead(current8Pin);

      current1Average += (current1-current1Average)/currentAverageRatio;
      current2Average += (current2-current2Average)/currentAverageRatio;
      current3Average += (current3-current3Average)/currentAverageRatio;
      current4Average += (current4-current4Average)/currentAverageRatio;
      current5Average += (current5-current5Average)/currentAverageRatio;
      current6Average += (current6-current6Average)/currentAverageRatio;
      current7Average += (current7-current7Average)/currentAverageRatio;
      current8Average += (current8-current8Average)/currentAverageRatio;

      if (sequence >= SEQ_REGULATING) {
        leftCurrentMax = max(max(current1, current2), max(current3, current4));
        rightCurrentMax = max(max(current5, current6), max(current7, current8));

        leftModulationPeakAverage += (analogRead(leftModulationPin)-leftModulationPeakAverage)/modulationPeakAverageRatio;
        rightModulationPeakAverage += (analogRead(rightModulationPin)-rightModulationPeakAverage)/modulationPeakAverageRatio;

        if (modulationPeak < leftModulationPeakAverage)
        {
          modulationPeak = leftModulationPeakAverage;
        }
        if (modulationPeak < rightModulationPeakAverage)
        {
          modulationPeak = rightModulationPeakAverage;
        }
      }

      // Calc regulators set point
      byte percentage = calcPercentSetPoint();
      if (percentageSetPoint != percentage){
        // New set point must be set
        pidSetPoint = percentage * (maximalRefCurrent - minimalRefCurrent) / 100 + minimalRefCurrent;
        percentageSetPoint = percentage;
        if (sequence >= SEQ_STARTING){
          regulate();
        }
      }

      // Average set points for slave regulators from master measure
      pid2SetPoint += (current1Average-pid2SetPoint)/pidSetPointSlaveRatio;
      pid4SetPoint += (current3Average-pid4SetPoint)/pidSetPointSlaveRatio;
      pid6SetPoint += (current5Average-pid6SetPoint)/pidSetPointSlaveRatio;
      pid8SetPoint += (current7Average-pid8SetPoint)/pidSetPointSlaveRatio;

      displayIndicator();
    }
  }

  switch (sequence)
  {
  case SEQ_DISCHARGE:
    // Discharging

    // Reset errors
    errorCause = NO_ERR;
    errorNumber = NO_ERR;

    // Pre-sequence
    if (sequenceStartTime == 0){
      resetRegulators();
      ledRed.Off();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    ledGreen.Execute(800, 200);

    // Diagnostic
    stepMaxTime = dischargeMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 0;
    stepCurValue = 1;

    if(elapsedTime > dischargeMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_DISHARGETOOLONG;
      break;
    }

    if(elapsedTime < dischargeMinTime || checkInRange(0, startCurrent) == CHECK_RANGE_TOOHIGH)
    {
      break;
    }

    // Post-sequence
    sendDatas();
    sequenceStartTime = 0;
    sequence++;

  case SEQ_HEAT:
    // Startup tempo

    // Pre-sequence
    if (sequenceStartTime == 0){
      resetRegulators();
      ledRed.Off();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    ledGreen.Execute(400, 400);

    // Diagnostic
    stepMaxTime = heatMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = elapsedTime;

    // Ensure no current at this step
    if(checkInRange(0, startCurrent + 10) == CHECK_RANGE_TOOHIGH)
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_CURRENTONHEAT;
      break;
    }

    if(elapsedTime < heatMaxTime)
    {
      break;
    }

    // Diagnostic, force 100%
    stepElapsedTime = heatMaxTime;
    stepCurValue = heatMaxTime;

    // Post-sequence
    sequenceStartTime = 0;
    ledGreen.Off();
    ledRed.On();
    measureTemperatures();
    sendDatas();
    sequence++;
    delay(500);

  case SEQ_STARTING:
    // Starting High Voltage

    // Pre-sequence
    if (sequenceStartTime == 0){
      relayOn();
      ledRed.Off();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    ledGreen.Execute(20, 400);

    // Diagnostic
    stepMaxTime = highVoltageMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(startingMinCurrent, 0, startingMinCurrent);

    if(elapsedTime > highVoltageMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGOUTOFRANGE;
      break;
    }

    if(elapsedTime < 5)
    {
      break;
    }

    // If target points not reached, continue to regulate
    if(checkInRange(startingMinCurrent, 0) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    sequenceStartTime = 0;
    sequence++;

  case SEQ_REGULATING:
    // Waiting for reg

    // Pre-sequence
    if (sequenceStartTime == 0){
      relayOn();
      initRegulators();
      ledRed.Off();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    ledGreen.Execute(20, 1500);

    // Diagnostic
    stepMaxTime = regulationMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold, pidSetPoint - regulationTreshold);

    if(elapsedTime > regulationMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      if (errorTime == 0){
        errorTime = currentTime;
      }

      if (currentTime - errorTime > errorMaxTime)
      {
        // Fail current error
        sequence = SEQ_FAIL;
        errorNumber = ERR_REGULATINGMAXREACHED;
        break;
      }
    } 
    else {
      errorTime = 0;
    }

    // If target points not reached, continue to regulate
    if(checkInRange(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    sendDatas();
    sequenceStartTime = 0;
    sequence++;

  case SEQ_FUNCTION:
    // Normal Function

    // Pre-sequence
    if (sequenceStartTime == 0){
      relayOn();
      initRegulators();
      ledRed.Off();
      ledGreen.Off();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    // Measure Temp
    if (currentTime - lastTempMeasureTime > tempMeasureMinTime){
      measureTemperatures();
    }

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 0;
    stepCurValue = 0;

    check = checkInRange(minCurrent, maxCurrent);
    if(check != CHECK_RANGE_OK)
    {
      if (errorTime == 0){
        errorTime = currentTime;
      }

      if (currentTime - errorTime > errorMaxTime)
      {
        // Fail current error
        sequence = SEQ_FAIL;
        errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_FUNCTIONMINREACHED : ERR_FUNCTIONMAXREACHED;
        break;
      }
    } 
    else {
      errorTime = 0;
    }

    if(checkInRange(pidSetPoint - functionTreshold, pidSetPoint + functionTreshold) != CHECK_RANGE_OK)
    {
      if (outOfRangeTime == 0){
        outOfRangeTime = currentTime;
      }

      if ((currentTime - outOfRangeTime) / 1000 > outOfRangeMaxTime)
      {
        // Fail out of range error
        sequence = SEQ_FAIL;
        errorNumber = ERR_FUNCTIONOUTOFRANGE;
        break;
      }
    }
    else{
      outOfRangeTime = 0;
    }
    break;

  default:
    // Fail, protect mode
    reset();

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;
    lastDiagnosticTime = 0;

    // Error indicator
    // Otherwise display error number or tube number
    ledGreen.Off();
    ledRed.Execute(250, displayTubeNumber ? errorCause : errorNumber, 1200);
  }

  // Diagnostic
  if (currentTime - lastDiagnosticTime > diagnosticSendMinTime) {
    sendDatas();
  }
}

